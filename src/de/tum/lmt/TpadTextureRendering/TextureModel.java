package de.tum.lmt.TpadTextureRendering;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import de.tum.lmt.textureRendering.R;
import de.tum.lmt.textureRendering.R.drawable;
import de.tum.lmt.textureRendering.R.id;
import de.tum.lmt.textureRendering.R.layout;
import nxr.tpad.lib.TPad;
import android.app.Activity;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/** This fragment handles the input events and also is used to output the tactile, audio and visual informations from the underlying texture model to the user. 
*   It links the functionalities of the objects for the display to the user, e.g. TextureHaptics is instantiated in order to control the tactile output capabilities of the tpad phone.
*   It also manages the onTouchEvents and distribute them accordingly to its objects, e.g. if a user touches the screen, the TextureSound object has to play an impact sound whereas the the TextureVibration object has to vibrate in a certain pattern.
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class TextureModel extends Fragment implements OnTouchEventListener
{
	private final String TAG 								= "TextureModel";

	
	
	private TPad mTpad 										= null;
	private int mCurrentFrequency 							= ConstantsClass.HAPTIC_FREQUENCY;
	
	public Context mContext 								= null;
	public TextureModel mTextureModel2 						= null;	
	
	public TextureDB mTextureDB 							= null;
	public Texture mCurrentTexture 							= null;
	
	public TextureHaptics hapticDisplay 					= null;
	public TextureImage imageDisplay 						= null;
	public TextureSound soundDisplay 						= null;
	public TextureVibration vibrationDisplay 				= null;
	public Vibrator mVibrator 								= null;
	
	// Android velocity tracking object, used in predicting finger position
	public VelocityTracker vTracker 						= null;
	// Velocity and position variables of the finger. Get updated at approx 60Hz when the user is touching	
	private static float vy 								= 0.0f;
	private static float vx 								= 0.0f;
	private static float px 								= 0.0f;
	private static float py 								= 0.0f;
	private double mCurrentVelocity 						= -1.0f;

	// Current state
	public boolean mIsTextureLoaded 						= false;
	public int mCurrentInteractionMode 						= ConstantsClass.EXPLORE;

	// progress display element
	public ProgressDialog progress 							= null;


	
	public int mMacroAmpl									= 0;    
	public int mMicroAmpl									= 0;    
	public int mFrictionAmpl								= 0;    	
	public int mMacroWeight									= 0;    	
	public int mMicroWeight									= 0; 
	public int mFrictionWeight								= 0;	
	public int mMicroDistr									= 0;    	
	public int mHardnes										= 0; 
	public int mDuration									= 0;		
	

	//private ArrayList<Uri> mArrayUriImages 					= null;
	

	
	
	
	/**
	 * 
	 * 
	 * 
	 * @param Android parameters to create view.
	 * @return
	 * 
	 * 
	 * 
	 */

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		
		View rootView = inflater.inflate(R.layout.fragment_layout, container, false);

		
		
		
		// A context variable is needed in the following cases:
		//  1.) ... to let recently created objects know what happened before during the program (every time you use new keyword you have to pass the current context
		//  2.) ... you want to access standard common resources -> e.g. SharedPreferences or even Toast messages
		//  3.) ... implicitely accessing components
		//
		// Differences between this, getActivity(), getContext(), getBaseContext(), Context
		//
		//  this -> used in an activity to directly access its context, can be omitted
		//  getBaseContext() -> also used in an activity, same as this
		//	
		// 	getContext() -> used in View objects: Returns the context the view is running in, through which it can access the current theme, resources, etc.   
		// 	                You will see that our TextureImage Class extends the View class so we could either use getContext() or simply pass the activities context via the constructor
		//
		//  getActivity() -> used in fragments: Return the Activity this fragment is currently associated with. You can also call YourActivity currActivity = (YourActivity) mContext; and then use currActivity to call specific methods from this activity
		//
		//  Context variable -> broadcast receivers for instance do not contain any context, so it has to be passed through the onReceive() Call
		mContext = getActivity();
		
		mTextureDB 			= new TextureDB();
		mCurrentTexture 	= null; 
		mIsTextureLoaded 	= false;

		progress 			= new ProgressDialog(mContext);
		progress.setMessage("Loading texture model");
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(false);
		progress.setCancelable(false);
		progress.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);		
		
		
		hapticDisplay 		= new TextureHaptics(mContext,this,mTpad, null);
		imageDisplay 		= new TextureImage(mContext,this, null);
		imageDisplay 		= (TextureImage) rootView.findViewById(R.id.view1);
		imageDisplay.setTextureModel(this);
		soundDisplay 		= new TextureSound(mContext,this, null);
		vibrationDisplay 	= new TextureVibration(mContext,this, null);

		getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
		

		loadHomeScreen(); 
		
		
		if (savedInstanceState != null) 
		{
	            // Restore last state for checked position.
	            String TextureLoaded = savedInstanceState.getString("SelectedTexture");
	            if(TextureLoaded != null) 
	            {
	            	Log.i(TAG,"We are here to load a new texture");
	            	this.loadTexture(TextureLoaded);
	            }
	            else 
	            {
	   			 	Log.i(TAG,"No texture model passed");
	   		 	}
	     }
		 else 
		 {
			 Log.i(TAG,"Empty Bundle");
		 }
		 

		
		 
		
		
		
		// set button listeners, can be put into xml in future
		
		 ImageButton homeButton = (ImageButton) rootView.findViewById(R.id.homeButton);
		 if(homeButton != null) 
		 {
			 homeButton.setOnClickListener(new OnClickListener()
			 {
				public void onClick(View v)
				{
					gotoHomeScreen(v);
				}
			});
		 }
		 ImageButton libraryButton = (ImageButton) rootView.findViewById(R.id.libraryButton);
		 if(libraryButton != null) 
		 {
			 libraryButton.setOnClickListener(new OnClickListener()
			 {
				public void onClick(View v) 
				{
					gotoLibraryScreen(v);
				}
			});
		 }
		 ImageButton scanButton = (ImageButton) rootView.findViewById(R.id.scanButton);
		 if(scanButton != null)
		 {
			 scanButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoScanActivity(v);
				}
			});
		 }
		 ImageButton settingsButton = (ImageButton) rootView.findViewById(R.id.settingsButton);
		 if(settingsButton != null) 
		 {
			 settingsButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoParametersActivity(v);
				}
			});
		 }
		 ImageButton infoButton = (ImageButton) rootView.findViewById(R.id.infoButton);
		 if(infoButton != null) 
		 {
			 infoButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoInfoScreen(v);
				}
			});
		 }
		 
		 ImageButton helpButton = (ImageButton) rootView.findViewById(R.id.helpButton);
		 if(helpButton != null) 
		 {
			 helpButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoHelpScreen(v);
				}
			});
		 }
		return rootView;
	}
	
	/**
	 * Pass TPad object to this fragment - pass it further to hapticDisplay class objects.
	 * @param TPad object.
	 * @return
	 * 
	 * 
	 * 
	 */
	public void setTpad(TPad tpad) 
	{
		mTpad = tpad;
		
		if(hapticDisplay != null) 
		{
			hapticDisplay.setTpad(tpad); 
		}
	}
	/**
	 * Pass display bitmap to this fragment - pass it further to Texture Image class objects. 
	 * @param Bitmap which schall be displayed on the screen.
	 * @return
	 * 
	 * 
	 */	
	public void setDisplayBitmap(Bitmap bit)
	{
		imageDisplay.setDisplayBitmap(bit);
	}
	/**
	 * Pass haptic bitmap to this fragment - pass it further to TextureHaptics class objects. 
	 * @param Bitmap which schall be felt via TPad Phone capabilities.
	 * @return
	 * 
	 * 
	 */
	public void setHapticBitmap(Bitmap bit)
	{
		hapticDisplay.setHapticBitmap(bit);
	}
	/**
	 * Pass vibration motor to this fragment.
	 * @param Vibration Motor object.
	 * @return
	 * 
	 * 
	 */
	public void setVibrator(Vibrator vibr)
	{
		mVibrator = vibr;
	}	
	
	
	
	






	

	

	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * This function and it overloading implementations take care of the texture loading.
	 * @param newTextureID The ID of the texture in the database. Per definition, this is the "name" (see Texture.class)
	 * @param displayImageUri Uri object referring to the selected image resource. We use this resource to select the image for the visual display.
	 */
	boolean loadTexture(Texture texture) 
	{
		if(texture == null) 
		{
			Log.e(TAG,"Loading of texture failed.");
			return false;
		}

		// Store the reference to the new texture
		mCurrentTexture = texture;


		// Set display image
		Uri temp  = Uri.fromFile(new File(mCurrentTexture.createImageBitmapName()));
		Log.i(TAG, "Loading image from file " + mCurrentTexture.createImageBitmapName());
		imageDisplay.setDisplayBitmap(temp);

		// Set haptic image and haptic parameters
		// Note: hapticDisplay accesses the haptic rendering parameters directly from mCurrentTexture
		hapticDisplay.setTexture(mCurrentTexture);


		// Set sound
		if(mCurrentTexture.mPlaySound) 
		{
			soundDisplay.loadSoundFile(mCurrentTexture.createSoundName());
			soundDisplay.loadImpactSoundFile(mCurrentTexture.createImpactSoundName());
		}
			
		// Set vibration actuator
		vibrationDisplay.setTexture(mCurrentTexture);
			
		// Indicate that texture has been loaded successfully.
		mIsTextureLoaded = true;
		progress.dismiss();	

		

		if(mCurrentTexture.mTextureDisplayName.compareTo("") != 0)
		{
			Toast.makeText(mContext,"", Toast.LENGTH_SHORT).cancel();
			Toast.makeText(mContext,mCurrentTexture.mTextureDisplayName + " (texture "+ (mTextureDB.currentLoadedTextureNumber+1) + "/" + mTextureDB.mTextures.size() + ")", Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(mContext,"", Toast.LENGTH_SHORT).cancel();
			Toast.makeText(mContext,"Now displaying the received texture...", Toast.LENGTH_SHORT).show();
		}
		return true;	
	}
	boolean loadTexture(String newTextureID) 
	{
		if(mTextureDB == null)
		{ 
			Log.i(TAG,"No texture DB initialized!"); 
			return false;
		}
		else 
		{
			return loadTexture(mTextureDB.getTextureByName(newTextureID));
		}

	}
	boolean loadTexture(String newTextureID, Uri displayImageUri) 
	{
		Log.i(TAG, "Loading texture with URI " + displayImageUri.toString());

		// Store the reference to the new texture
		if(mTextureDB == null) 
		{ 
			Log.i(TAG,"No texture DB initialized!"); 
			return false;
		}
		else 
		{
			return loadTexture(mTextureDB.getTextureByName(newTextureID));
		}
	}

	
	

	/**
	 * 
	 * @param number of how many texture entries to go next or previous.
	 * @return Has texture been successfully loaded?
	 * 
	 *  if the user uses two fingers to switch between textures, this function will be called for loading previous or next textures.
	 *  
	 */
	public boolean loadNextTexture(int offset) 
	{
		int currentTextureNumber = mTextureDB.currentLoadedTextureNumber;
		currentTextureNumber += offset;
		
		if(offset > 0)
		{
			if(currentTextureNumber == mTextureDB.mTextures.size()) 
				currentTextureNumber = 0;
		}
		else
		{
			if(currentTextureNumber < 0 ) 
				currentTextureNumber = mTextureDB.mTextures.size()-1;
		}
		
		Texture newTexture = mTextureDB.getTextureByNumber(currentTextureNumber);
		
		if(newTexture != null) 
		{
			return loadTexture(newTexture);
		} 
		else 
		{
			return false;
		}	
	}

	
	/**
	 * 
	 * @param Paths to resources in order to display/feel/hear texture.
	 * @return
	 * 
	 * This loadTexture version is called, when we have received a remote texture and have to load it from our device.
	 * 
	 */
	public void loadTextureFromFiles(String mDisplayBitmapPath, String mMacroDisplayPath, String mSoundPath, String mImpactPath, String mReadFromParametersFile) 
	{
		String[] parts = mReadFromParametersFile.split("#");

		Texture newTexture = new Texture();
	
		try 
		{
			newTexture.mTextureName 			= "display";
			newTexture.mAmplMacroRough    		= Float.parseFloat(parts[0]);//1.0f;//(float)jsonObject.getDouble("AmplitudeMacroRoughness");
			newTexture.mAmplMicroRough    		= Float.parseFloat(parts[1]);//0.5f;//(float)jsonObject.getDouble("AmplitudeMicroRoughness");
			newTexture.mAmplFric          		= Float.parseFloat(parts[2]);//0.5f;//(float)jsonObject.getDouble("AmplitudeFriction"); 
			newTexture.mMicroRoughDistribution = Float.parseFloat(parts[3]);//0.5f;//(float)jsonObject.getDouble("DistributionMicroRoughness");	
			newTexture.mWeightMacroRough  		= Float.parseFloat(parts[4]);//1.0f;//(float)jsonObject.getDouble("WeightMacroRoughness");
			newTexture.mWeightMicroRough  		= Float.parseFloat(parts[5]);//0.0f;//(float)jsonObject.getDouble("WeightMicroRoughness");
			newTexture.mWeightFric        		= Float.parseFloat(parts[6]);//0.0f;//(float)jsonObject.getDouble("WeightFriction");
			newTexture.mAmplHardness      		= Float.parseFloat(parts[7]);//0.9f;//(float)jsonObject.getDouble("AmplitudeHardness");
			newTexture.mDurationHardness  		= Long.parseLong(parts[8]);//200L;// jsonObject.getLong("DurationHardness");

		}
		catch(NumberFormatException e) 
		{
			Log.e(TAG,"Error while parsing parameter file: number exception.");
			Toast.makeText(mContext,"", Toast.LENGTH_SHORT).cancel();
			Toast.makeText(mContext,"Invalid parameters received", Toast.LENGTH_LONG).show();
		}
		// Set the paths to the files
		newTexture.mTextureDisplayImageFilePath 	= mDisplayBitmapPath;
		newTexture.mTextureHapticImageFilePath 		= mMacroDisplayPath;
		newTexture.mSoundPath 						= mSoundPath;
		newTexture.mImpactSoundPath 				= mImpactPath;
		newTexture.mTextureDisplayName 				= "Received Image";

		Log.i(TAG, "Texture Display Image Path 		= " + mDisplayBitmapPath);
		Log.i(TAG, "Texture Haptic Image Path 		= " + mMacroDisplayPath);
		Log.i(TAG, "Texture Sound Path 				= " + mSoundPath);
		Log.i(TAG, "Texture Impact Sound Path 		= " + mImpactPath);
		
		// If sound is recorded with steel tooltip, it does not sound realistically as if the user touches a surface with the bare finger, so we do not replay the sound 
		newTexture.mPlaySound = false;


		loadTexture(newTexture);
	}
	/**
	 * Reset current texture objects attributes.
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	public void unloadTexture() 
	{
		mIsTextureLoaded = false;
		// Reset the texture data
		mCurrentTexture = null;
		hapticDisplay.setTexture(null);
		vibrationDisplay.setTexture(null);
		mTextureDB.currentLoadedTextureNumber = -1;
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Clear screen and go back to home screen.
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */
	public void loadHomeScreen()
	{
		// Load the instruction image
		try 
		{
			// Check for screen orientation
			Bitmap defaultBitmap;
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				defaultBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.backgroundstart_comparison);
			}
			else
			{
				defaultBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.backgroundstart);
			}
			
			imageDisplay.setDisplayBitmap(defaultBitmap);
			defaultBitmap.recycle();
		}
		catch (Exception e)
		{
			Log.e(TAG, "Could not load instruction image: " + e.toString());
		}
	}
	


	
	
	
	
	
	
	
	
	


	/**
	 * 
	 * @param MotionEvent
	 * @return
	 * 
	 * Main Touch even method. Gets called whenever Android reports a new touch event. We distribute it to the according subclasses in order to have a logical structure of handling these touch events.
	 * 
	 */
	public boolean onTouchEvent(MotionEvent event) 
	{
		// Set the current interaction state according to the number of fingers in contact
		if(event.getPointerCount() == 1)
			mCurrentInteractionMode = ConstantsClass.EXPLORE;
		if(event.getPointerCount() == 2 && this.mIsTextureLoaded)
			mCurrentInteractionMode = ConstantsClass.ZOOM;
		if(event.getPointerCount() == 3)
			mCurrentInteractionMode = ConstantsClass.CHANGE;


		// Get position coordinates, and scale for proper dataBitmap size
		px = event.getX();
		py = event.getY();

	

		// Switch by which type of event occured
		switch (event.getActionMasked()) 
		{

			// Case where user first touches down on the screen
			case MotionEvent.ACTION_DOWN:
	
				// Reset velocities to zero
				vx = 0.0f;
				vy = 0.0f;
	
				// Start a new velocity tracker
				if (vTracker == null) 
				{
					vTracker = VelocityTracker.obtain();
				} 
				else 
				{
					vTracker.clear();
				}
	
				// Add first event to tracker
				vTracker.addMovement(event);
	
				
				
				onFingerDownEvent(vx, vy, px, py);
				// Image display is called for one, two or three fingers, because it handles scaling, dragging and so on
				imageDisplay.onFingerDownEvent(vx, vy, px, py);
				if(mCurrentInteractionMode == ConstantsClass.EXPLORE) 
				{
					// Only if a single finger is in contact
					hapticDisplay.onFingerDownEvent(vx, vy, px, py);
					soundDisplay.onFingerDownEvent(vx, vy, px, py);
					vibrationDisplay.onFingerDownEvent(vx, vy, px, py);
				}
				if(mCurrentInteractionMode == ConstantsClass.ZOOM) 
				{
					soundDisplay.stopSound();
					vibrationDisplay.stop();
				}
	
				break;
	
				
				
				
				
			case MotionEvent.ACTION_MOVE:
	
				// Add new motion even to the velocity tracker
				vTracker.addMovement(event);
	
				// Compute velocity in pixels/ms
				vTracker.computeCurrentVelocity(1);
	
				// Get computed velocities, and scale them appropriately
				vx = vTracker.getXVelocity(); 
				vy = vTracker.getYVelocity(); 
	
				//Log.i(TAG,String.valueOf(this.mIsMultiTouchActive));
	
				onFingerMoveEvent(vx, vy, px, py);	 
				imageDisplay.onFingerMoveEvent(vx, vy, px, py);
	
				boolean checkIfInsideFragment = true;
				if(px < 0.0f || ( (float)this.getView().getMeasuredWidth()-px) < 0.0f) 
					checkIfInsideFragment = false;
	
				if(mCurrentInteractionMode == ConstantsClass.EXPLORE && checkIfInsideFragment) 
				{
					soundDisplay.onFingerMoveEvent(vx, vy, px, py);
					vibrationDisplay.onFingerMoveEvent(vx, vy, px, py);
					hapticDisplay.onFingerMoveEvent(vx, vy, px, py);
				}
				else if(mCurrentInteractionMode == ConstantsClass.EXPLORE && !checkIfInsideFragment && mTextureModel2 != null) 
				{
					soundDisplay.stopSound();
					mTextureModel2.soundDisplay.onFingerMoveEvent(vx, vy, px, py);
					mTextureModel2.vibrationDisplay.onFingerMoveEvent(vx, vy, px, py);
					mTextureModel2.hapticDisplay.onFingerMoveEvent(vx, vy, px, py);
				}
				
				if(mCurrentInteractionMode == ConstantsClass.ZOOM)
				{
					soundDisplay.stopSound();
					vibrationDisplay.stop();
				}
	
	
				break;
	
			case MotionEvent.ACTION_UP:
				onFingerUpEvent(vx, vy, px, py);
				
				imageDisplay.onFingerUpEvent(vx, vy, px, py);
				hapticDisplay.onFingerUpEvent(vx, vy, px, py);
				
				if(mCurrentInteractionMode == ConstantsClass.EXPLORE)
				{
					soundDisplay.onFingerUpEvent(vx, vy, px, py);
					vibrationDisplay.onFingerUpEvent(vx, vy, px, py);
				}
			
	
				break;
	
				
				
				
			case MotionEvent.ACTION_CANCEL:
				// Recycle velocity tracker on a cancel event
				vTracker.recycle();
				//set to null after recycle to prevent illegal state
				vTracker = null;
				break;
		}

		return true;
	}
	@Override
	public void onFingerDownEvent(double vx, double vy, double px, double py)
	{

	}
	/**
	 * Calculate the current finger velocity.
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */	
	@Override
	public void onFingerMoveEvent(double vx, double vy, double px, double py)
	{
		mCurrentVelocity = Math.sqrt(vx*vx + vy*vy);	

	}
	/**
	 * Reset current finger velocity.
	 * @param
	 * @return
	 * 
	 * 
	 */	
	@Override
	public void onFingerUpEvent(double vx, double vy, double px, double py)
	{
		Toast.makeText(mContext,"", Toast.LENGTH_SHORT).cancel();
		mCurrentVelocity = 0;
	}
	public double getCurrentVelocity() 
	{
		return mCurrentVelocity;
	}






	
	
	
	
	
	

	

	/**
	 * 
	 * @param
	 * @return
	 * 
	 * save current dynamic state of mCurrentTexture. This is important, when the device is rotated because the fragment/activity will be destroyed and resumed.
	 */
	@Override
    public void onSaveInstanceState(Bundle outState) 
	{
        super.onSaveInstanceState(outState);
        if(mCurrentTexture != null) 
        {
        	outState.putString("SelectedTexture", this.mCurrentTexture.getTextureID());
        }
    }
	/**
	 * 
	 * @param View
	 * @return
	 * 
	 * Wrapper for calling necessary functions in order to leave current screen.
	 * 
	 */
	public void gotoHomeScreen(View v)
	{
		TextureRendererActivity activity = (TextureRendererActivity) mContext;
		activity.leaveFullscreen();
		
		unloadTexture();
		loadHomeScreen();
	}

	/**
	 * 
	 * Send intent to go to info screen.
	 * 
	 * @param View
	 * @return
	 * 
	 * 
	 * 
	 * 
	 */
	public void gotoInfoScreen(View v)
	{
		Intent intent = new Intent(mContext, InfoActivity.class);
		startActivityForResult(intent, ConstantsClass.REQ_CODE_INFO_ACTIVITY);
	}
	
	/**
	 * Send intent to go to info screen.
	 * @param View
	 * @return
	 * 
	 * 
	 * 
	 */
	public void gotoHelpScreen(View v)
	{
		Intent intent = new Intent(mContext, HelpActivity.class);
		startActivityForResult(intent, ConstantsClass.REQ_CODE_HELP_ACTIVITY);
	}		
	/**
	 * Open library window.
	 * @param View
	 * @return
	 * 
	 * 
	 * 
	 */
	public void gotoLibraryScreen(View v)
	{
		selectPicture();
	}
	/**
	 * Go to Texture Recognizer App. This is disabled for now.
	 * @param View
	 * @return
	 * 
	 * 
	 * 
	 */
	public void gotoScanActivity(View v)
	{

	}	

	/**
	 * 
	 * @param View
	 * @return
	 * 
	 * 
	 * This function starts the configuration/parameters activity and listens to its result
	 * Since we are passing an own created object (here:Texture object) we need to serialize the data and attach it to a bundle, which will be send to the activity
	 * 
	 * 
	 */
	public void gotoParametersActivity(View v)
	{
		
		Texture currentTexture = mCurrentTexture;
		if(currentTexture != null)
		{
			Intent intent = new Intent(mContext, ParametersActivity.class);
			Bundle bundle = new Bundle(); 
			bundle.putSerializable("CurrentTexture",currentTexture);
			intent.putExtras(bundle);
			intent.putExtra("HapticDisplay", imageDisplay.mShowHapticTextureData);
			// start activity and listen to its results
			startActivityForResult(intent, ConstantsClass.REQ_CODE_PARAMETER_ACTIVITY);
		}
		else 
		{
			//There is no texture loaded: show a message to the user
			Toast.makeText(mContext,"", Toast.LENGTH_SHORT).cancel();
			Toast.makeText(mContext,"Please load a texture first!!", Toast.LENGTH_LONG).show();
		}
	}
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * This function starts the filebrowser and image chooser android activities
	 * 
	 * 
	 * 
	 */
	public void selectPicture()
	{
		// open android file browser, here search specifically for images
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		Intent chooser = Intent.createChooser(intent, "Choose a Visual Layer");
		//chooser.putExtra("ViewID", 15);
		startActivityForResult(chooser, ConstantsClass.REQ_CODE_PICK_IMAGE);
	}

	
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * This function manages the incoming intents from various activities
	 * 
	 * 
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		

		if(resultCode == Activity.RESULT_OK) 
		{ 
			if(data.getIntExtra("MenuSelection", -1) != -1) 
			{
				switch(data.getIntExtra("MenuSelection",-1)) 
				{
					case ConstantsClass.REQ_CODE_HELP_ACTIVITY: 		gotoHelpScreen(null); 			break;
					case ConstantsClass.REQ_CODE_PICK_IMAGE: 			gotoLibraryScreen(null);		break;
					case ConstantsClass.REQ_CODE_SCAN_ACTIVITY: 		gotoScanActivity(null); 		break;
					case ConstantsClass.REQ_CODE_INFO_ACTIVITY: 		gotoInfoScreen(null); 			break;
					case ConstantsClass.REQ_CODE_HOME_SCREEN: 			gotoHomeScreen(null); 			break;
					case ConstantsClass.REQ_CODE_PARAMETER_ACTIVITY: 	gotoParametersActivity(null); 	break;
				}
				return;
			}
		}

		// Parse the activity result
		switch (requestCode) 
		{

		case ConstantsClass.REQ_CODE_PICK_IMAGE:

			
			if(resultCode == Activity.RESULT_OK && data.getIntExtra("MenuSelection", -1) == ConstantsClass.REQ_CODE_PICK_IMAGE) 
			{
				gotoLibraryScreen(null);
			}
			
			
			if (resultCode == Activity.RESULT_OK && data != null) 
			{
				//mTextureModel.progress.show();
				
				Uri bitmapUri = data.getData();

				// Calculate the texture ID from the filename of the image
				String newTextureID = TextureUtility.getFileName(bitmapUri, mContext);
				newTextureID = newTextureID.toLowerCase();
				newTextureID = TextureUtility.removeExtension(newTextureID);
				
				if (bitmapUri != null)
				{

					this.progress.show();
					boolean loadSuccessful = this.loadTexture(newTextureID, bitmapUri);
					//goFullscreen();
					if(!loadSuccessful) 
					{
						Log.e(TAG, "Could not load texture corresponding to the selected image");
						Toast.makeText(mContext,"", Toast.LENGTH_SHORT).cancel();
						Toast.makeText(mContext,"There is no texture model associated with this image!", Toast.LENGTH_LONG).show();
						progress.dismiss();
					}
				}
				else 
				{
					Log.e(TAG, "BitmapURI is null");
				}

			}
			break;

			/*
			 * Result from Parameter Activity, get slider positions	
			 */
		case ConstantsClass.REQ_CODE_PARAMETER_ACTIVITY:

			if(resultCode == Activity.RESULT_OK)
			{
				mMacroAmpl 			= data.getIntExtra("MacroAmplitude", 0);
				mMicroAmpl 			= data.getIntExtra("MicroAmplitude", 0);
				mFrictionAmpl 		= data.getIntExtra("FrictionAmplitude", 0);
				mMacroWeight 		= data.getIntExtra("MacroWeight", 0);
				mMicroWeight 		= data.getIntExtra("MicroWeight", 0);
				mFrictionWeight 	= data.getIntExtra("FrictionWeight", 0);
				mMicroDistr 		= data.getIntExtra("MicroDistr", 0);
				mHardnes 			= data.getIntExtra("Hardness", 0); 
				mDuration 			= data.getIntExtra("Duration", 0);	


				setTextureParameters(mMacroAmpl, mMicroAmpl, mFrictionAmpl, mMacroWeight, mMicroWeight, mFrictionWeight,mMicroDistr,mHardnes,mDuration);  
				imageDisplay.setShowHapticTextureData(data.getBooleanExtra("HapticDisplay", false));			
			}

			break;
			
			
		case ConstantsClass.REQ_CODE_INFO_ACTIVITY:
			if(resultCode == Activity.RESULT_OK)
			{
				Log.i(TAG,"Arrived here" +  data.getIntExtra("MenuSelection", -1));
			}
			break;
		}
	}
	/**
	 * 
	 * This function saves the setting changes in the parameters activity to our current mTexture in this fragment.
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	public void setTextureParameters(int macroAmpl,int microAmpl, int frictionAmpl, int macroWeight, int microWeight, int frictionWeight, int microDistr, int hardness, int duration)
	{

		mCurrentTexture.mAmplMacroRough    		= (float)(macroAmpl/100.0f);    
		mCurrentTexture.mAmplMicroRough    		= (float)(microAmpl/100.0f);    
		mCurrentTexture.mAmplFric          		= (float)(frictionAmpl/100.0f);  
		mCurrentTexture.mMicroRoughDistribution = (float)(microDistr/100.0f);

		mCurrentTexture.mWeightMacroRough  		= (float)(macroWeight/100.0f);    	
		mCurrentTexture.mWeightMicroRough  		= (float)(microWeight/100.0f); 
		mCurrentTexture.mWeightFric        		= (float)(frictionWeight/100.0f);

		mCurrentTexture.mAmplHardness      		= (float)(hardness/100.0f);
		mCurrentTexture.mDurationHardness  		= (long)(duration*3L);


		//mTextureDB.setTexture(mCurrentTexture);
	}
	
	
	
	
	
	
	
	
	
	
	@Override
	public void onResume() 
	{
		super.onResume();
	}
	@Override
    public void onActivityCreated(Bundle savedInstanceState) 
	{
        super.onActivityCreated(savedInstanceState); 
	}

	@Override
	public void onStop() 
	{
		super.onStop();
	}
	
	@Override
	public void onDestroy() 
	{
		imageDisplay.recycleImages();
		hapticDisplay.recycleImages();		
		super.onDestroy();
	}	

}
