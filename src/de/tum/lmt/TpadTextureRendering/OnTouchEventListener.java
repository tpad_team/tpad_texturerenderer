package de.tum.lmt.TpadTextureRendering;

public interface OnTouchEventListener
{
    public void onFingerDownEvent(double vx, double vy, double px, double py);
    public void onFingerMoveEvent(double vx, double vy, double px, double py);
    public void onFingerUpEvent(double vx, double vy, double px, double py);
}
