/*
 * Copyright 2014 TPad Tablet Project. All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL ARSHAN POURSOHI OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied.
 */

package de.tum.lmt.TpadTextureRendering;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import de.tum.lmt.textureRendering.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/** This is the launcher activity for the texture renderer application.
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/

public class TextureRendererActivity extends ParentActivity implements Serializable 
{
	
	private final String TAG 							= "TextureRendererActivity";
	private static final long serialVersionUID 			= 1L;

	private static TPad mTpad							= null;
	private RemoteCommunication mRemoteCommunication	= null;
	private Vibrator mVibrator							= null;		
	
	private ArrayList<Uri> mArrayUriImages 				= null;
	private View mDecorView								= null;
	private Uri mUri									= null;	
	private String[] mOptionTitles						= null;
	private DrawerLayout mDrawerLayout					= null;
	private ListView mDrawerList						= null;	
	private Handler mHandler 							= null;

	private ActionBarDrawerToggle mDrawerToggle			= null;
	private CharSequence mDrawerTitle					= null;
	private CharSequence mTitle;


	private boolean mOpenCvLoaded 						= false;
	private boolean selectPicturesFirstTime 			= true;

	public int mMacroAmpl								= 0;    
	public int mMicroAmpl								= 0;    
	public int mFrictionAmpl							= 0;    	
	public int mMacroWeight								= 0;    	
	public int mMicroWeight								= 0; 
	public int mFrictionWeight							= 0;	
	public int mMicroDistr								= 0;    	
	public int mHardnes									= 0; 
	public int mDuration								= 0;	

	public final String MacroAmplitudeKey 				= "MacroAmplitude";
	
	public String mCurrentImageName						= null;
	public String mCurrentSoundName						= null;
	public String mCurrentImpactName					= null;
	public String mCurrentTextureName					= null;

	
	
	
	
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
	
		super.onCreate(savedInstanceState);	
		// sets the corresponding xml layout file for this view
		setContentView(R.layout.pictureloader_main);
		
		mOptionTitles 		= getResources().getStringArray(R.array.options_array);
		mArrayUriImages 	= new ArrayList<Uri>();
		
		// new handler for runnable thread. Since it is created in this activity, it can pass data to the main thread (UI thread) in order to share data.
		mHandler			= new Handler();	
		

		// initialize tpad functionality
		mTpad 				= new TPadImpl(this);
		// get vibration motor context from system, vibrator has to be enabled in manifest file as well (uses-permission)
		mVibrator 			= (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);	
		
		// initialize fragments for this activity
		initializeFragments();
		
		
		
		// Check for OpenCV
		Log.i(TAG, "Trying to load OpenCV library");
		if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_7, this, mOpenCVCallBack)) 
		{
			Log.e(TAG, "Cannot connect to OpenCV Manager");
		}
		
		
		
		
		// instantiate new Bluetooth Listener 
		mRemoteCommunication= new RemoteCommunication(mTextureModel);
		mRemoteCommunication.StartObservingBluetooth();		

		// Same as onNewIntent
		// Check, if we start and receive data via bluetooth
		Bundle extras = getIntent().getExtras();
		if( (extras != null) && (!mTextureModel.mIsTextureLoaded) ) 
		{
			Log.i(TAG,"OnCreate " + extras.getString("fileImageDisplay"));
			mTextureModel.loadTextureFromFiles(extras.getString("fileImageDisplay"), extras.getString("fileImageHaptic"), 	extras.getString("fileSoundMove"), 	extras.getString("fileSoundImpact"),extras.getString("fileParameters"));
		}
	}
	/**
	 * If you want to store variables during the app destruction (for instance while rotating it) save them in the instance state bundle.
	 */
	/*@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		
		//outState.putInt(MacroAmplitudeKey, mMacroAmpl);
		super.onSaveInstanceState(outState);
		
		//Use following lines in OnCreate() to restore the saved value:
		//if (savedInstanceState != null) 
		//{
	    //    // Restore value of members from saved state
	    //    mMacroAmpl = savedInstanceState.getInt(MacroAmplitudeKey);
	    //} 
	    //else 
	    //{
	    //    // Probably initialize members with default values for a new instance
	    //}
	
	}*/

	
	/**
	 * 
	 * @param
	 * @return
	 *  This function initializes either one or two fragments with its own tpad functionalities, depending on the orientation of the android device.
	 *  Note: In the android manifest, change android:screenOrientation="user"  to android:screenOrientation="portrait"  to disable the rotation of the device.
	 *  
	 */
	private void initializeFragments() 
	{

		// Are we initially (onCreate) in landscape mode?...
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			// initialize new TextureModelFragment
			TextureModel fragment = (TextureModel) getFragmentManager().findFragmentById(R.id.textureLandscape1);	
			
			if(fragment != null) 
			{
				mTextureModel = fragment;
				mTextureModel.setTpad(mTpad);
				mTextureModel.setVibrator(mVibrator);
				Toast.makeText(this,"", Toast.LENGTH_LONG).cancel();
				Toast.makeText(this,"Hint: douple tap on the screen to load two textures at once!", Toast.LENGTH_LONG).show();
			
			}
			else 
			{
				Log.e(TAG,"Fragment landscape 1 was not found!");
			}

			
			fragment = (TextureModel) getFragmentManager().findFragmentById(R.id.textureLandscape2);	
			// We are in portrait mode
			if(fragment != null)
			{
				mTextureModel2 = fragment;
				mTextureModel2.setTpad(mTpad);
				mTextureModel2.setVibrator(mVibrator);
			}
			else 
			{
				Log.e(TAG,"Fragment landscape 2 was not found!");
			}
			
			if(mTextureModel != null && mTextureModel2 != null)
			{
				mTextureModel.mTextureModel2 = mTextureModel2;
				mTextureModel2.mTextureModel2 = mTextureModel;
			}
			
		}
		// ...else go initially to portrait mode. The portrait mode only uses 
		else
		{
			TextureModel fragment = (TextureModel) getFragmentManager().findFragmentById(R.id.texturePortrait1);

			if(fragment != null)
			{
				mTextureModel = fragment;
				mTextureModel.setTpad(mTpad);
				mTextureModel.setVibrator(mVibrator);
				mTextureModel.mTextureModel2 = null;
				mTextureModel2 = null;
			}
			else
			{
				Log.e(TAG,"Fragment was not found!");
			}
		}
	}
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 *  This function listens for incoming intent.
	 *  In our case, it reacts on an incoming bluetooth intent from RemoteCommunication Class and loads the according texture model related to the passed parameters(stored in bundle).
	 *  
	 */
	@Override
	protected void onNewIntent(Intent intent) 
	{
		super.onNewIntent(intent);
		
		Bundle extras = intent.getExtras();
	    if(extras != null)
	    {
	        if(extras.containsKey("fileImageDisplay"))
	        {
	        	Log.i(TAG,"Set texture according to received files from RemoteCommunication.");
	        	this.mTextureModel.loadTextureFromFiles(extras.getString("fileImageDisplay"), extras.getString("fileImageHaptic"), extras.getString("fileSoundMove"), extras.getString("fileSoundImpact"),extras.getString("fileParameters"));
	        }
	    }
	}
	
	

	/**
	 * 
	 * @param
	 * @return
	 *  This function instantiates a new timed thread inside the main activity that is fired each 100ms. In our case it can be used to display the current user finger velocity (disabled now) or to check for incoming files via bluetooth.
	 *  
	 *  
	 */
	private Runnable mUpdateClockTask = new Runnable() 
	{
		public void run() 
		{
			String title = "Velocity " + String.format("%.2f", mTextureModel.getCurrentVelocity());
			// you may print or log the current velocity now
			
			mHandler.postDelayed(mUpdateClockTask, 100);
			
			Context cont = getBaseContext();
			mRemoteCommunication.CheckForNewReceivedFiles(cont);
		}
	};



	/**
	 * 
	 * @param
	 * @return
	 *  This function checks, if OpenCV is correctly loaded.
	 *  
	 *  
	 */
	private BaseLoaderCallback mOpenCVCallBack = new BaseLoaderCallback(this) 
	{
		@Override
		public void onManagerConnected(int status) 
		{
			switch (status) 
			{
			case LoaderCallbackInterface.SUCCESS: 
			{
				Log.e("TEST", "Connected to OpenCV Manager");
				Mat tempMat = new Mat(10, 10, CvType.CV_8UC4);
				mOpenCvLoaded = true;
			}
			break;
			default: 
			{
				super.onManagerConnected(status);
				Log.i(TAG, "default loaded");
			}
			break;
			}
		}
	};






	

	/**
	 * 
	 * @param
	 * @return
	 * 
	 */	
	

	public void selectPictures()
	{
		
		if(selectPicturesFirstTime) 
		{
			new AlertDialog.Builder(this).setMessage("Press long on a texture to select it. You can select up to two textures at once. Then press okay!").setCancelable(false).setPositiveButton("Okay!", new DialogInterface.OnClickListener() 
			{
	            public void onClick(DialogInterface dialog, int id) 
	            {
	        		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
	        		intent.setType("image/*");
	        		intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
	        		startActivityForResult(Intent.createChooser(intent,"Select Pictures"), ConstantsClass.REQ_CODE_PICK_IMAGES);
	            }
	        }).show();
			
			selectPicturesFirstTime = false;
		}
		else 
		{
			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    		intent.setType("image/*");
    		intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
    		startActivityForResult(Intent.createChooser(intent,"Select Pictures"), ConstantsClass.REQ_CODE_PICK_IMAGES);
		}
		
		
		
	}


	/**
	 * Dispatch an intent to take a picture of a surface (not used).
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */
	public void dispatchTakePictureIntent()
	{
		Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) 
		{
			Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
			File f = new File(Environment.getExternalStorageDirectory(),  "photo.jpg");
			i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
			mUri = Uri.fromFile(f);
			startActivityForResult(i, ConstantsClass.TAKE_PICTURE);
		}
	}

	/**
	 * Ir startActivityForResult() was called, the incoming intent from an extern activity is handled here.
	 * @param
	 * @return
	 * 
	 * 
	 */
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);

		// Parse the activity result
		switch (requestCode) 
		{

		case ConstantsClass.REQ_CODE_PICK_IMAGES:
	
			if (resultCode == RESULT_OK && data != null) 
			{
				Uri singleUri = data.getData();
		
				// If there is only one image selected...
				if(singleUri != null) 
				{
					
					// Calculate the texture ID from the filename of the image
					String newTextureID = TextureUtility.getFileName(singleUri, this);
					newTextureID = newTextureID.toLowerCase();
					newTextureID = TextureUtility.removeExtension(newTextureID);
	
					boolean loadSuccessful = false;
					Log.i(TAG,"Loading single image");

					if(mTextureModel != null && mTextureModel2 != null) 
					{
						// Load as texture 1, if there is currently no texture loaded
						if(!mTextureModel.mIsTextureLoaded)
						{
							loadSuccessful = mTextureModel.loadTexture(newTextureID, singleUri);
						}
						// Load as texture 2, if there is currently no texture loaded
						else if(!mTextureModel2.mIsTextureLoaded) 
						{
							loadSuccessful = mTextureModel2.loadTexture(newTextureID, singleUri);
						}
						// Overwrite texture 1
						else 
						{
							loadSuccessful = mTextureModel.loadTexture(newTextureID, singleUri);
						}

					}

					if(!loadSuccessful) 
					{
						Log.e(TAG, "Could not load texture corresponding to the selected image");
						Toast.makeText(this,"", Toast.LENGTH_LONG).cancel();
						Toast.makeText(this,"There is no texture model associated with this image!", Toast.LENGTH_LONG).show();
					}
					break;		
				}
				
				// In this case there are at least 2 images selected
				if(data.getClipData()!=null)
				{
					ClipData mClipData=data.getClipData();
					mArrayUriImages.clear(); // clearing mArray
					Log.v(TAG, "Selected " + mClipData.getItemCount() + " Images");
					
					
					// There are more than two images selected: notify the user!
					if(mClipData.getItemCount() > 2) 
					{
						Toast.makeText(this,"", Toast.LENGTH_LONG).cancel();
						Toast.makeText(this,"Please select no more than two images!", Toast.LENGTH_LONG).show();
						
						break;	
					}
	
					Log.i(TAG,"Loading two images");
					
					for(int i=0; i<mClipData.getItemCount(); i++) 
					{
						ClipData.Item item = mClipData.getItemAt(i);
						Uri uri = item.getUri();
						mArrayUriImages.add(uri);
						
						// Calculate the texture ID from the filename of the image
						String newTextureID = TextureUtility.getFileName(uri, this);
						newTextureID = newTextureID.toLowerCase();
						newTextureID = TextureUtility.removeExtension(newTextureID);

						// Now we have the texture ID ready. Try to load this texture from the database
						if (uri != null)
						{
							boolean loadSuccessful = false;
							if(i==0)
								loadSuccessful = mTextureModel.loadTexture(newTextureID, uri);
							if(i==1)
								loadSuccessful = mTextureModel2.loadTexture(newTextureID, uri);
							
							if(!loadSuccessful) 
							{
								Log.e(TAG, "Could not load texture corresponding to the selected image");
								Toast.makeText(this,"", Toast.LENGTH_LONG).cancel();
								Toast.makeText(this,"There is no texture model associated with this image!", Toast.LENGTH_LONG).show();		
							}
							else 
							{
								Toast.makeText(this,"", Toast.LENGTH_LONG).cancel();
								Toast.makeText(this,"Two textures have been loaded!", Toast.LENGTH_LONG).show();
							}
						}
					}
				}
				else 
				{
					Log.i(TAG, "clipDataNull");
				}
			}
			break;
		}
	}

	

	
	/**
	 * 
	 *  This function simply catches, if we are in landscape or portrait mode of the android device and displays toast messages correspondingly.
	 *  Note: In the android manifest, change android:screenOrientation="user"  to android:screenOrientation="portrait"  to disable the rotation of the device.
	 * @param
	 * @return	  
	 *  
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) 
	{
	    super.onConfigurationChanged(newConfig);

	    Log.i(TAG,"onConfigurationChanged");
	    
	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
	    {
	    	//setFragmentsForLandscape();
	        Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
	    } 
	    else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
	    {
	    	//setFragmentsForPortrait();
	        Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
	    }
	}	


	


	
	
	
	
	@Override
	public void setTitle(CharSequence title) 
	{
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) 
	{
		super.onPostCreate(savedInstanceState);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{

		int id = item.getItemId();
		if (id == android.R.id.home)
		{
			gotoHomeScreen(null);		
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		if(mTextureModel.mIsTextureLoaded) 
		{
			goFullscreen();
		}
		// stop the run thread...
		mHandler.removeCallbacks(mUpdateClockTask);
		// ... and restart it at 100 ms
		mHandler.postDelayed(mUpdateClockTask, 100);
	}
	@Override
	public void onPause() 
	{
		super.onPause();
		// stop the run thread
		mHandler.removeCallbacks(mUpdateClockTask);
	}
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		// always make sure to turn the tpad off
		mTpad.turnOff();
		//mTpad.disconnectTPad();
		mRemoteCommunication.stopCommunication();
	}
}
