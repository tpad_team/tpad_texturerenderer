package de.tum.lmt.TpadTextureRendering;

import de.tum.lmt.textureRendering.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;


/**  
*
* This Activity shows the help screen if the user clicks on the help button.
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class HelpActivity extends ParentActivity  
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		createButtonListeners();
	}

}
