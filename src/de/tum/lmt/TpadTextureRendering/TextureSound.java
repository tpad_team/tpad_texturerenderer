package de.tum.lmt.TpadTextureRendering;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import de.tum.lmt.textureRendering.R;
import de.tum.lmt.textureRendering.R.raw;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;


/** This class handles all sound related events that happen, whenever the user touches the screen. 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/

public class TextureSound  implements OnTouchEventListener
{
	
	private final String TAG 							= "TextureSound";
	
	private Context mContext							= null;
	private TextureModel mTextureModel 					= null;

	private SoundPool mSoundPool 						= null;;
	boolean mSoundPoolLoaded 							= false;
	private int mCurrentSoundID							= 0;
	private int mCurrentStream 							= 0;
	
	private double leftVolume 							= 0.5f;
	private double rightVolume 							= 0.5f;
	
	
	
	private SoundPool mSoundPoolImpact 					= null;
	boolean mSoundPoolLoadedImpact 						= false;
	private int mCurrentImpactID						= 0;
	private int mCurrentStreamImpact					= 0;
	
	private String mImpact								= null;
	private int mImpactID 								= R.raw.g2crushedrock_impact;
	private double leftVolumeImpact 					= 0.80f;
	private double rightVolumeImpact 					= 0.80f;	
	private float mRate									= 1.00f;
	
	
	// set initial dummy sounds
	public String[] mNameSounds 						= {"g1rhombaluminummesh_soundtpad"	};
	public String[] mNameImpacts 						= {"g1rhombaluminummesh_impact"		};		
	
	
	
	private boolean mPlaying 							= false;
	private long mTimeLastSoundPlay 					= 0;
	private long mTimeOutPlay 							= 15000;


	private long mTimeLastImpactPlay 					= 0;
	private long mTimeOutImpactPlay 					= 50;	
	
	
	File mNewSoundFile 									= null;
	File mNewImpactFile 								= null;
	
	
	
	
	
	
	/**
	 * Set up sound pools for impact sounds as well as movement sounds.
	 * @param 
	 * @return
	 */
	public TextureSound(Context context, TextureModel texModel,AttributeSet attrs) 
	{
		mContext = context;
		mTextureModel = texModel;
		
		// we create two sound pools, one for impact sound and one for movement sound
		mSoundPoolImpact 	= new SoundPool(mNameImpacts.length, AudioManager.STREAM_MUSIC, 0);		
		mSoundPool 			= new SoundPool(mNameSounds.length,  AudioManager.STREAM_MUSIC, 0);

		mSoundPoolImpact.setOnLoadCompleteListener(new OnLoadCompleteListener() 
		{
	        @Override
	        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) 
	        {
	        	mSoundPoolLoadedImpact = true;
	        	Log.i(TAG, "Sound Pool Impact initiated.");
	        }
	    });			
		mSoundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() 
		{
	        @Override
	        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) 
	        {
	        	mSoundPoolLoaded = true;
	        	Log.i(TAG, "Sound Pool Movement initiated.");
	        }
	    });
	}

		
	
	

	
	
	
	
	
	
	/**
	 * Try to load a movement sound resource from the local storage system.
	 * @param Path to sound file on SD card.
	 * @return
	 */
	public void loadSoundFile(String soundPath) 
	{
		mNewSoundFile = new File(soundPath);
		Log.i(TAG, "New sound to be added: " + soundPath);
		
		if(mNewSoundFile != null)
		{
			try 
			{			
				FileInputStream fileInputStream = new FileInputStream(mNewSoundFile);
				FileDescriptor fd;
				
				fd = fileInputStream.getFD();
				
				mSoundPoolImpact.unload(mCurrentImpactID);
				mCurrentSoundID = mSoundPool.load(fd, 0, mNewSoundFile.length(), 1);
				
				fileInputStream.close();
				
			} 
			catch (IOException e) 
			{
				Log.e(TAG, "Could not create Sound File Descriptor.");
				
				// In this case there is no corresponding sound
				mCurrentSoundID = -1;
			}
		}
		else
		{
			Log.e(TAG, "New Sound File is null.");
		}
		
	}

	/**
	 * Try to load an impact sound resource from the local storage system.
	 * @param Path to sound file on SD card.
	 * @return
	 */
	public void loadImpactSoundFile(String impactPath) 
	{
		mNewImpactFile = new File(impactPath);
		Log.i(TAG, "New sound to be added: " + impactPath);
		
		if(mNewImpactFile != null)
		{
			try 
			{			
				FileInputStream fileInputStream = new FileInputStream(mNewImpactFile);
				FileDescriptor fd;

				fd = fileInputStream.getFD();
				
				mSoundPoolImpact.unload(mCurrentImpactID);
				mCurrentImpactID = mSoundPoolImpact.load(fd, 0, mNewImpactFile.length(), 1);
				
				fileInputStream.close();
				
			} 
			catch (IOException e) 
			{
				Log.e(TAG, "Could not create Impact File Descriptor.");
				mCurrentImpactID = -1;
			}
		}
		else
		{
			Log.e(TAG, "New Impact File is null.");
		}
		
	}	
	
	
	
	
	/**
	 * Try to play a movement sound resource according to the current stream ID (mCurrentStream)
	 * @param velocity
	 * @return
	 */
	public void playMovementSound(double velocity) 
	{
		
		if (!mSoundPoolLoaded)
		{
			Log.e(TAG,"SoundPool not loaded.");
			//Toast.makeText(mContext.getApplicationContext(), "SoundPool not loaded", Toast.LENGTH_SHORT).show();
			return;
		}
		
		if(mCurrentSoundID < 0) 
		{
			Log.i(TAG,"No sound loaded!");
			return;
		}
		
		if(mPlaying)
		{
			return;
		}		

		// if finger moves faster, play sound up to 10% faster
		mRate = 1.0f;
	

		leftVolume = velocityToSoundAmplitude(velocity);
		rightVolume = leftVolume;
		
		

		
		// timeout for playing next movement sound
		long currentTime = System.currentTimeMillis();
		if ((currentTime - mTimeLastSoundPlay) > mTimeOutPlay) 
		{
			mCurrentStream 			= mSoundPool.play(mCurrentSoundID, 1.0f,1.0f, 5, -1, 1.0f); 
			mPlaying 				= true;
			mTimeLastSoundPlay 		= System.currentTimeMillis();
		}
	}
	

	
	/**
	 * Try to play an impact sound resource according to the current stream ID (mCurrentStream)
	 * @param velocity
	 * @return
	 */
	
	public void playImpact() 
	{
		
		if (!mSoundPoolLoadedImpact) 
		{
			Log.e(TAG,"SoundPoolImpact not loaded");
			return;
		}
		
		if(mCurrentImpactID < 0) 
		{
			Log.i(TAG,"No impact sound loaded!");
			return;
		}
		
		// timeout for playing next movement sound
		long currentTime = System.currentTimeMillis();
		if ((currentTime - mTimeLastImpactPlay) > mTimeOutImpactPlay) 
		{
			// play again
			mSoundPoolImpact.stop(mCurrentImpactID);
			mCurrentStreamImpact 	= mSoundPoolImpact.play(mCurrentImpactID, (float)leftVolumeImpact,(float)rightVolumeImpact, 1, 0, 1.0f); 
			mTimeLastImpactPlay 	= System.currentTimeMillis();
		}

	}
	
	/**
	 * Stop the currently played sound, if it is playing at the moment.
	 */
	public void stopSound() 
	{
		if (mSoundPoolLoaded && mPlaying) 
		{
			mSoundPool.stop(mCurrentStream);
			//mVibrator.cancel(); // TODO do we want to stop vibrating immediately?
			mPlaying = false;
			mTimeLastSoundPlay = 0;
		}
		mSoundPoolImpact.stop(mCurrentImpactID);
	}
	
	
	
	
	/**
	 * Implementation of the OnTouchEvents: Sound, where it will distinguish, if to play an impact or a movement sound.
	 * @param velocity
	 * @return
	 */
	public void onFingerDownEvent(double vx, double vy, double px, double py) 
	{	
		if(mTextureModel.mIsTextureLoaded && mTextureModel.mCurrentInteractionMode == ConstantsClass.EXPLORE) 
		{
			playImpact(); 
		}	
	}
	public void onFingerMoveEvent(double vx, double vy, double px, double py) 
	{
		if(mTextureModel.mIsTextureLoaded && mTextureModel.mCurrentInteractionMode == ConstantsClass.EXPLORE) 
		{
			if(mTextureModel.getCurrentVelocity() > 0.3)
			{
				playMovementSound(mTextureModel.getCurrentVelocity());
			}
			else
			{
				stopSound();
			}
		}
	}
	public void onFingerUpEvent(double vx, double vy, double px, double py) 
	{
		stopSound();
	}
	
	
	
	

	

	
	/**
	 * Map the finger velocity to a sound amplitude according to the user velocity - this is assumed to be somehow logarithmic (Loudness!).
	 * @param velocity
	 * @return
	 */
	private float velocityToSoundAmplitude(double velocity)
	{
		float loudness = (float)(100.0f * Math.log10(1+3*velocity));
		if(velocity < 0.05) 
		{
			loudness = 0.0f;
			
		}
		if(loudness > 100.0f) 
		{
			
			loudness = 100.0f;
		}
		loudness /= (100.0f);
		
		Log.i(TAG, "Loudness" + loudness);
		return 1.0f;

	}
}
