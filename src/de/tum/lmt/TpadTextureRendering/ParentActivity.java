package de.tum.lmt.TpadTextureRendering;

import de.tum.lmt.textureRendering.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;





/** 
*
* This activity is the super class for our used activities, the other activities will inherit from it to implement options button and full screen mode.
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class ParentActivity extends Activity  
{

	protected TextureModel mTextureModel;
	protected TextureModel mTextureModel2;
	private View mDecorView;
	

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		mDecorView = getWindow().getDecorView();
		mTextureModel = new TextureModel();
	
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		if (id == android.R.id.home)
		{
			if(this.getComponentName().getClassName().compareTo("de.tum.lmt.textureRendering.TextureRendererActivity") != 0)
				finish();			
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private void generateIntent(int selection) 
	{
		Intent intent = new Intent();
		intent.putExtra("MenuSelection",selection);
		setResult(RESULT_OK, intent);    
	}
	
	
	
	public void gotoHomeScreen(View v)
	{
		generateIntent(ConstantsClass.REQ_CODE_HOME_SCREEN);
		leaveFullscreen();
		finish();
	}
	
	
	
	public void gotoLibraryScreen(View v)
	{
		generateIntent(ConstantsClass.REQ_CODE_PICK_IMAGE);
		finish();
	}	
	
	
	public void gotoScanActivity(View v)
	{
		generateIntent(ConstantsClass.REQ_CODE_SCAN_ACTIVITY);
		finish();
	}	
	

	public void gotoParametersActivity(View v)
	{
		generateIntent(ConstantsClass.REQ_CODE_PARAMETER_ACTIVITY);
		finish();

	}
	
	

	public void gotoInfoScreen(View v)
	{
		// Only if we are currently not in this activity
		if(this.getClass().getSimpleName().compareTo("InfoActivity") != 0) 
		{
			generateIntent(ConstantsClass.REQ_CODE_INFO_ACTIVITY);
			finish();
		}
	}	
	
	
	
	public void gotoHelpScreen(View v)
	{
		// Only if we are currently not in this activity
		if(this.getClass().getSimpleName().compareTo("HelpActivity") != 0) 
		{
			generateIntent(ConstantsClass.REQ_CODE_HELP_ACTIVITY);
			finish();
		}	
	}	
	

	public void goFullscreen()
	{
		mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_IMMERSIVE);
	}
	

	public void leaveFullscreen()
	{
		mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_IMMERSIVE);
	}
		
		
	protected void createButtonListeners()
	{
		
		ImageButton homeButton = (ImageButton) this.findViewById(R.id.homeButton);
		 Log.i("TEST","Button " + homeButton);
		 if(homeButton != null) 
		 {
			 homeButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoHomeScreen(v);
				}
			});
		 }
		 
		 
		 ImageButton libraryButton = (ImageButton) this.findViewById(R.id.libraryButton);
		 if(libraryButton != null) 
		 {
			 libraryButton.setOnClickListener(new OnClickListener()
			 {
				public void onClick(View v) 
				{
					gotoLibraryScreen(v);
				}
			});
		 }
		 
		 ImageButton scanButton = (ImageButton) this.findViewById(R.id.scanButton);
		 if(scanButton != null) 
		 {
			 scanButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v)
				{
					gotoScanActivity(v);
				}
			});
		 }
		 
		 
		 ImageButton settingsButton = (ImageButton) this.findViewById(R.id.settingsButton);
		 if(settingsButton != null) 
		 {
			 settingsButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoParametersActivity(v);
				}
			});
		 }
		 
		 
		 ImageButton infoButton = (ImageButton) this.findViewById(R.id.infoButton);
		 if(infoButton != null) 
		 {
			 infoButton.setOnClickListener(new OnClickListener() 
			 {
				public void onClick(View v) 
				{
					gotoInfoScreen(v);
				}
			});
		 }
		 
		 ImageButton helpButton = (ImageButton) this.findViewById(R.id.helpButton);
		 if(helpButton != null) 
		 {
			 helpButton.setOnClickListener(new OnClickListener()
			 {
				public void onClick(View v) 
				{
					gotoHelpScreen(v);
				}
			});
		 }

	}

}
