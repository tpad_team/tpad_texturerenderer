package de.tum.lmt.TpadTextureRendering;

import de.tum.lmt.textureRendering.R;
import android.app.Activity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


/** 
*
* This activity shows the info screen, where the user can click on the last text view to follow a link. 
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/

public class InfoActivity extends ParentActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		createButtonListeners();
		
		
		TextView link = (TextView) findViewById(R.id.infoLink);
		link.setMovementMethod(LinkMovementMethod.getInstance());
	}
	
}
