package de.tum.lmt.TpadTextureRendering;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

import android.util.Log;



/** Description of Class 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class PPMM_FileIO
{
	
	/*
	 * 
	 */

	
	/*
	 * 
	 */
	protected FileInputStream mFileInputStream		= null;
	protected FileOutputStream mFileOutputStream 	= null;
	
	protected RandomAccessFile mRandomAccessFile 	= null;
	
	/*
	 * 
	 */
	protected File mFile 							= null;
		
	protected FileReader mFileReader 				= null;
	protected FileWriter mFileWriter 				= null;
	protected PrintWriter mPrintWriter 				= null;
	protected Scanner mScanner 						= null;
		
	/*
	 * 
	 */
	protected BufferedReader mBufferedReader 		= null;
	protected BufferedWriter mBufferedWriter 		= null;
	
	
	
	/*
	 *  Serialization
	 */
	ObjectOutputStream mObjectOutputStream 			= null;
	ObjectInputStream  mObjectInputStream 			= null;
	
	
	
	protected class DataObject implements Serializable
	{
		private static final long serialVersionUID = 1L;
		
		protected ArrayList<Byte> mHeader;
		protected ArrayList<Byte> mData;	
		
		public DataObject()
		{
			mHeader = new ArrayList<Byte>();
			mData   = new ArrayList<Byte>();
		}
		public DataObject(ArrayList<Byte> h,ArrayList<Byte> d)
		{
			mHeader = h;
			mData   = d;
		}		
	}
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * Text File Operations
	 */
	public void writeTxtFile(String path, String[] text) throws FileNotFoundException
	{
		if(mFile == null)
		{
			mFile = new File(path);
			mPrintWriter = new PrintWriter(mFile);
			
			for(String line:text)
			{
				mPrintWriter.println(line);
			}
			
			mPrintWriter.close();
			mPrintWriter = null;
			mFile = null;
		}
	}
	public ArrayList<String> readTxtFile(String path) throws FileNotFoundException
	{
		ArrayList<String> text = new ArrayList<String>();
		
		if(mFile == null)
		{
			mFile = new File(path);
			mScanner = new Scanner(mFile);
			
			while(mScanner.hasNextLine())
			{
				text.add(mScanner.nextLine());
			}	
			
		}
		
		return text;
	}
	public void readTxtFileWithOtherDelimiter(String path, String delimiter) throws FileNotFoundException
	{
		if(mScanner == null)
		{
			mScanner = new Scanner(new BufferedReader(new FileReader(path)));
		
			mScanner.useDelimiter(delimiter);
			while(mScanner.hasNext())
			{
				// do something with words...
				
				if(mScanner.hasNextDouble())
				{
					// sum up the doubles
				}
				else if (mScanner.hasNextLine())
				{}
				else if (mScanner.hasNext("specialPatternYouAreLookingFor"))
				{}
				// default
				else
				{
					Log.i("PPMM_FileIO",mScanner.next());
				}
			}
			mScanner.close();
			mScanner = null;
		}
	}
	

	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	public void serializeData(String path, ArrayList<Byte> header,ArrayList<Byte> data) throws IOException
	{
		if(mFile == null)
		{
			mFile = new File(path);
			ArrayList <DataObject> dataObjects = new ArrayList<DataObject>();
			dataObjects.add(new DataObject(header,data));
			
			if(mFileOutputStream == null)
			{
				mFileOutputStream = new FileOutputStream(mFile);
				
				mObjectOutputStream = new ObjectOutputStream(mFileOutputStream);
				
				for (DataObject dO:dataObjects)
				{
					mObjectOutputStream.writeObject(dO);
				}
				mObjectOutputStream.close();
				mFileOutputStream.close();
			}
		}
	}
	public void deserializeData(String path) throws IOException, ClassNotFoundException
	{
		if(mFile == null)
		{
			mFile = new File(path);
			
			if(mFileInputStream == null)
			{
				mFileInputStream = new FileInputStream(mFile);
				mObjectInputStream = new ObjectInputStream(mFileInputStream);

				ArrayList<DataObject> dataObjects = new ArrayList<DataObject>();
				
				try
				{
					while(true)
					{
						DataObject d = (DataObject)mObjectInputStream.readObject();
						dataObjects.add(d);
					}
				}
				catch(EOFException ex)
				{
					mObjectInputStream.close();
					mFileInputStream.close();					
				}

			}
		}
	}	
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	public void writeBytesToFile(String path, byte[] byteValues) throws IOException
	{
		if((mFileOutputStream == null) && (byteValues != null) )
		{
			try 
			{			
				mFileOutputStream = new FileOutputStream(path);
				mFileOutputStream.write(byteValues);
			}
			finally
			{
				mFileOutputStream.close();
			}
		}
	}
	public byte[] readBytesFromFile(String path) throws IOException
	{
		byte[] byteValues = null;
		if(mRandomAccessFile == null)
		{
			try 
			{			
				mRandomAccessFile = new RandomAccessFile(path,"rw");
				byteValues = new byte[(int)mRandomAccessFile.length()];
				mRandomAccessFile.read(byteValues);
			}
			finally
			{
				mFileOutputStream.close();
			}
		}
		return byteValues;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
}
