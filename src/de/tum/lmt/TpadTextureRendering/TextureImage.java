package de.tum.lmt.TpadTextureRendering;

import ioio.lib.spi.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Toast;



/** This class takes care of the entire visual calculations and rendering and inherits from the View class. 
*  The display bitmaps for the visual display can be found underthe path /sdcard/ImageDataNew/  . This path is defined in ConstantsClass (also check the ConstantsClass variables for other fixed paths).
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/

public class TextureImage extends View  implements OnTouchEventListener 
{	
	private final String TAG 										= "TextureImage";
	
	private Context mContext										= null;

	private TextureModel mTextureModel								= null;

	
	
	private int mHeight 											= 0;
	private int mWidth 												= 0;

	public Bitmap mDisplayBitmap									= null;
	public Bitmap mDisplayBitmapScaled 								= null;

	public Paint mDataPaint											= null;


	// Settings:
	public boolean mShowTextureImage 								= true; 	// Show an image at all?
	public boolean mShowHapticTextureData 							= false;	// Show the underlying haptic macroscopic data image instead of the colored image 


	// For multitouch gesture recognition
	private ScaleGestureDetector mScaleDetector 					= null;
	private GestureDetector mGestureDetector 						= null;
	private GestureDetector.OnDoubleTapListener doubleTapListener 	= null;



	private float dragStartX 										= 0.0f;
	private float dragStartY 										= 0.0f;
	private float dragTranslateX 									= 0.0f;
	private float dragTranslateY 									= 0.0f;
	private float dragPreviousTranslateX 							= 0.0f;
	private float dragPreviousTranslateY 							= 0.0f;
	private boolean dragResizeNecessary 							= false;
	private boolean changeToNextTexture 							= false;
	private boolean changeToLastTexture 							= false;
	private static float changeTextureSwipeLength 					= 300.0f;
	private static float changeTextureVelocityLimit 				= 1.5f;
	private static float changeTextureScaleLimit					= 1.4f;
	
	private float mVisualScaleFactorOriginal 						= (float) 1.0f;
	private float mVisualScaleFactorInput							= (float) 1.0f;

	//public float mDisplayScaleFactor 								= 1.0f;	

	private static float MIN_ZOOM 									= 1.0f;
	private static float MAX_ZOOM 									= 3.0f;

	private float focalPointY 										= 0.0f;
	private float focalPointX 										= 0.0f;
	public Rect mViewRectangle										= null;

	
	
	
	
	
	
	
	
	
	
	/**
	 * Initialize the needed variables for this class.
	 * @param
	 * @return
	 * 
	 * 
	 */
	public TextureImage(Context context, AttributeSet attrs) 
	{
		super(context, attrs);

		
		
		mContext 	= context;
		mHeight 	= 1000;
		mWidth 		= 1000;


		// Set default bitmap to 10x10 square, just so we don't get null pointer
		// exceptions
		Bitmap defaultBitmap = Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
		setDisplayBitmap(defaultBitmap);
		defaultBitmap.recycle();

		// Set some graphic properties flags of the dataBitmap since it will
		// also be shown to the screen
		mDataPaint 			= new Paint();
		mDataPaint.setColor(Color.MAGENTA);
		mDataPaint.setAntiAlias(true);

		// Multitouch gesture detections
		mScaleDetector 		= new ScaleGestureDetector(context, new ScaleListener());
		mGestureDetector	= new GestureDetector(context, new GestureListener());

		// Initialize the view rectangle (sets all corners to 0)
		mViewRectangle = new Rect();	

	}
	public TextureImage(Context context, TextureModel texModel,AttributeSet attrs )
	{
		super(context, attrs);
		mContext = context;
		mTextureModel = texModel;
	}

	
	public void setTextureModel(TextureModel texModel) 
	{
		this.mTextureModel = texModel;
	}

	
	
	
	
	
	
	
	/**
	 * 
	 * Set the visual (display) bitmap to the screen according to a URI.
	 * 
	 * @param displayBitmap URI
	 * @return 
	 * 
	 * 
	 *  
	 *  
	 */
	public void setDisplayBitmap(Uri displayImageUri) 
	{
		Bitmap newBitmap;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;

		try 
		{
			ParcelFileDescriptor parcelFileDescriptor = mContext.getContentResolver().openFileDescriptor(displayImageUri, "r");
			FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor(); 
			newBitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
			parcelFileDescriptor.close();
			// Set this as the image display bitmap
			this.setDisplayBitmap(newBitmap);
		}
		catch (Exception e) 
		{
			Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();
			Toast.makeText(mContext,"Display image not found: " + e.toString(), Toast.LENGTH_LONG).show();

		}

	}
	/**
	 * 
	 * Set the visual (display) bitmap to the screen according to a bitmap.
	 * 
	 * @param displayBitmap bmp
	 * @return 
	 */	
	public void setDisplayBitmap(Bitmap bmp) 
	{

		mVisualScaleFactorOriginal 	= bmp.getWidth() / (float) mWidth;
		// Create new bitmap from a copy of the reference. This ensures the
		// reference copy won't be used, and it can then be destroyed.
		mDisplayBitmap 				= bmp.copy(Bitmap.Config.ARGB_8888, true);


		resetScaleFactors();
		createScaledBitmap(mDisplayBitmap, mVisualScaleFactorOriginal);

		// Invalidate calls the onDraw() function of the view class, when will
		// draw the newly copied dataBitmap to the screen
		invalidate();

	}
	
	
	/**
	 * 
	 * @param enable to show the underlying grayscale haptic bitmap.
	 * @return
	 * 
	 * 
	 *  This function is called by the parameters activity and enables/disables the rendering of either the display bitmap or the haptic bitmap to the user.
	 *  
	 *  
	 */
	public void setShowHapticTextureData(boolean bool)
	{
		mShowHapticTextureData = bool;
		invalidate();
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param canvas
	 * @return
	 * 
	 * 
	 *  Main view drawing function. Handles scaling, translating and in our case also either drawing the colored display image or the grayscaled haptic image.
	 *  
	 *  
	 */
	@Override
	protected void onDraw(Canvas canvas) 
	{
		super.onDraw(canvas);

		// Draw a background color in case of overdraw on the screen. Mostly for
		// error checking purposes
		canvas.drawColor(Color.DKGRAY);

		// Scale the canvas according to the user input
		// Note: we do not change the underlying bitmap, but only "zoom" in. There will be
		// visual artifacts due to the zooming if we go too far. But this is smoother than
		// recreating a new bitmap from the large original bitmap at every draw.
		canvas.scale(mVisualScaleFactorInput, mVisualScaleFactorInput,focalPointX,focalPointY);

		// Translate the canvas according to the user input
		canvas.translate(dragTranslateX, dragTranslateY);

		// Extract the bounds of the currently rendered canvas. The rectangle r denotes the area
		// of the bitmap that is currently shown to the user. We use this information as soon as
		// the multitouch event ends to cut off a high-res haptic image from the original haptic image
		// for the force rendering.
		canvas.getClipBounds(mViewRectangle);

		
		// Synchronize the visual and haptic input scale factors! 
		if(mTextureModel != null) 
		{
			mTextureModel.hapticDisplay.synchronizeHapticScaleFactorWithVisualScaleFactor(mVisualScaleFactorInput);
		}



		if (mShowTextureImage) 
		{
			if (mShowHapticTextureData && mTextureModel.hapticDisplay.mHapticBitmapScaled != null) 
			{
				canvas.drawBitmap(mTextureModel.hapticDisplay.mHapticBitmapScaled, 0, 0, mDataPaint);
			} 
			else 
			{
				canvas.drawBitmap(mDisplayBitmapScaled, 0, 0, mDataPaint);
			}
		}
	}
	/**
	 * Calls Garbage Collection explicitly for a bitmap.
	 * @param
	 * @return
	 */
	public void recycleImages()
	{
		if(mDisplayBitmap != null)
		{
			mDisplayBitmap.recycle();
		}
		
		if(mDisplayBitmapScaled != null)
		{
			mDisplayBitmapScaled.recycle();
		}
	}	
	
	
	
	
	
	
	
	
	
	





	
	
	
	
	
	
	

	
	/**
	 * 
	 * @param Bitmap and according scale factor.
	 * @return
	 * 
	 * 
	 *  Create a bitmap either scaled or unscaled.
	 *  
	 *  
	 */
	private void createScaledBitmap(Bitmap bit, float factor)
	{
		if(bit != null) 
		{
			mDisplayBitmapScaled = Bitmap.createScaledBitmap(bit, (int) (bit.getWidth() / factor), (int) (bit.getHeight() / factor), false);
		}
		else 
		{
			mDisplayBitmapScaled = Bitmap.createBitmap(720, 844, Bitmap.Config.ARGB_8888);
		}
	}
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 *  Reset the scale factor to the original value. 
	 *  
	 *  
	 */
	private void resetScaleFactors() 
	{

		if(mTextureModel != null)
		{
			mTextureModel.hapticDisplay.resetScaleFactor(mWidth);
		}
		if(mDisplayBitmap != null) 
		{
			mVisualScaleFactorOriginal = mDisplayBitmap.getWidth() / (float) mWidth;
		}
		this.mVisualScaleFactorInput 	= 1.0f;
		this.dragTranslateX 			= 0.0f;
		this.dragTranslateY 			= 0.0f;
		this.dragPreviousTranslateX 	= 0.0f;
		this.dragPreviousTranslateY 	= 0.0f;
		this.dragStartX 				= 0.0f;
		this.dragStartY 				= 0.0f;
	}







	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * Passed by TextureModel fragment - handles the visual output whenever the user zooms in, drags the screens or leaves the screen with the finger(s) (multitouch).
	 *  
	 *  
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{

		// Just call the touch event handler of the general texture model class
		mTextureModel.onTouchEvent(event);

		// Forward this event to the scale detector
		if(mTextureModel.mCurrentInteractionMode == ConstantsClass.ZOOM) 
		{
			mScaleDetector.onTouchEvent(event);
		}

		// Forward this event to the gesture detector
		mGestureDetector.onTouchEvent(event);

		switch (event.getActionMasked())
		{

			case MotionEvent.ACTION_DOWN: 
			{
	
				dragStartX = event.getX() - dragPreviousTranslateX*mVisualScaleFactorInput;
				dragStartY = event.getY() - dragPreviousTranslateY*mVisualScaleFactorInput;
	
				break;
			}
	
			case MotionEvent.ACTION_MOVE: 
			{
				// Translate only if we are currently in the ZOOM mode
				if(mTextureModel.mCurrentInteractionMode == ConstantsClass.ZOOM) 
				{
	
					dragTranslateX = ( event.getX() - dragStartX)/mVisualScaleFactorInput;
					dragTranslateY = ( event.getY() - dragStartY)/mVisualScaleFactorInput;					
	
					dragResizeNecessary = true;
				}
	
				break;
			}
			case MotionEvent.ACTION_UP: 
			{
	
				// Check the borders and reset if necessary
				if(dragResizeNecessary) 
				{
					if(mViewRectangle.left < 0) 
						dragTranslateX+= (float) mViewRectangle.left;
					
					if(mViewRectangle.top < 0) 
						dragTranslateY+= (float) mViewRectangle.top;
					
					if(mDisplayBitmapScaled.getWidth() - mViewRectangle.right < 0) 
						dragTranslateX-= (float) (mDisplayBitmapScaled.getWidth()  - mViewRectangle.right);
					
					if(mDisplayBitmapScaled.getHeight() - mViewRectangle.bottom < 0) 
						dragTranslateY-= (float) (mDisplayBitmapScaled.getHeight() - mViewRectangle.bottom);
	
					dragPreviousTranslateX = dragTranslateX;
					dragPreviousTranslateY = dragTranslateY;
	
					mTextureModel.hapticDisplay.scale(mViewRectangle);
	
					dragResizeNecessary = false;
				}
	
	
				break;
			}
		}


		invalidate();	
		return true;
	}
	public void onFingerDownEvent(double vx, double vy, double px, double py) 
	{

	}
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * A user may swipe a texture beyond a border (left/right) to load the previous or next from the database.
	 * 
	 * 
	 */
	public void onFingerMoveEvent(double vx, double vy, double px, double py) 
	{

		if(mTextureModel.mIsTextureLoaded && mTextureModel.mCurrentInteractionMode == ConstantsClass.ZOOM) 
		{
			
			if(dragTranslateX  <  - changeTextureSwipeLength && Math.abs(vx) > changeTextureVelocityLimit && !changeToNextTexture && mVisualScaleFactorInput <= changeTextureScaleLimit)
			{
				mTextureModel.progress.show();
				changeToNextTexture = true;
				dragTranslateX = 0;
				dragTranslateY = 0;
				Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();
				mTextureModel.soundDisplay.stopSound();
			}

			if(dragTranslateX  >  changeTextureSwipeLength && Math.abs(vx) > changeTextureVelocityLimit && !changeToLastTexture && mVisualScaleFactorInput <= changeTextureScaleLimit) 
			{
				mTextureModel.progress.show();
				changeToLastTexture = true;
				dragTranslateX = 0;
				dragTranslateY = 0;
				Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();
				mTextureModel.soundDisplay.stopSound();
			}
		}
		
		
		double velocity =  Math.sqrt(vx*vx+vy*vy);
		//Log.i(TAG, "velocity " + velocity);
	}
	public void onFingerUpEvent(double vx, double vy, double px, double py)
	{
		//Log.i(TAG, "translate x: " + dragTranslateX);

		// Notify the user that dragging was too slow
		if(Math.abs(dragTranslateX)  >  changeTextureSwipeLength && !changeToNextTexture) 
		{

			if(Math.abs(vx) < changeTextureVelocityLimit) 
			{
				Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();
				Toast.makeText(mContext,"Swipe faster to change texture!", Toast.LENGTH_LONG).show();
				
			}
			
			if(mVisualScaleFactorInput > changeTextureScaleLimit) 
			{
				Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();
				Toast.makeText(mContext,"Zoom out to browse textures!", Toast.LENGTH_LONG).show();
			}

		}
		if(changeToNextTexture) 
		{
			changeToNextTexture = false;
			dragResizeNecessary = false;
			Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();
			mTextureModel.loadNextTexture(+1);
			
		}
		if(changeToLastTexture) 
		{
			changeToLastTexture = false;
			dragResizeNecessary = false;
			Toast.makeText(mContext,"", Toast.LENGTH_LONG).cancel();

			mTextureModel.loadNextTexture(-1);
			
		}
	}



	
	
	
	
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * Method getting called when the view changes dimensions. Used in the
	 * background for most purposes. 
	 *  
	 *  
	 */

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
		mWidth = MeasureSpec.getSize(widthMeasureSpec);
		mHeight = MeasureSpec.getSize(heightMeasureSpec);
		Log.i(TAG, "New Measure: " + String.valueOf(mWidth) + "w " + String.valueOf(mHeight) + "h");

		// The width of the view has change. First reset the scale factors and the calculate a new resized bitmap
		if(mDisplayBitmap != null) {
			resetScaleFactors();
			createScaledBitmap(mDisplayBitmap, mVisualScaleFactorOriginal);

			if(mTextureModel != null) {
				mTextureModel.hapticDisplay.resetScaleFactor(mWidth); }
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * The ScaleListener detects user two finger scaling and scales image.
	 * 
	 */
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener 
	{
		@Override
		public boolean onScaleBegin(ScaleGestureDetector detector) 
		{
			//mTextureModel.mIsMultiTouchActive = true;
			focalPointX = detector.getFocusX();
			focalPointY = detector.getFocusY();

			return true;
		}

		@Override
		public boolean onScale(ScaleGestureDetector detector) 
		{
			mVisualScaleFactorInput *= detector.getScaleFactor();
			mVisualScaleFactorInput = Math.max(MIN_ZOOM, Math.min(mVisualScaleFactorInput, MAX_ZOOM));


			invalidate();
			return true;
		}

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) 
		{	
			super.onScaleEnd(detector);
		}
	}


	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * Gesture Listener detects a single click, a fling event, a scroll event, a double tap or a long-holded click and passes that on
	 * to the view's listener.
	 *
	 */
	private class GestureListener extends GestureDetector.SimpleOnGestureListener 
	{
		/*
		@Override
		public boolean onSingleTapConfirmed(MotionEvent e)
		{
			if(doubleTapListener != null) 
			{
				return doubleTapListener.onSingleTapConfirmed(e);
			}
			return performClick();
		}
		*/

		@Override
		public void onLongPress(MotionEvent e)
		{
			//TextureRendererActivity activity = (TextureRendererActivity) mContext;
			//activity.gotoParametersActivity(mTextureModel.getView());
			//Log.i(TAG, "Long press event.");
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			//Log.i(TAG, "Fling event.");
			return true;
		}

		@Override
		public boolean onScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) 
		{
			//Log.i(TAG, "Scroll event.");
			return true;

		}

		@Override
		public boolean onDoubleTap(MotionEvent e) 
		{
			
			TextureRendererActivity activity = (TextureRendererActivity) mContext;
			
			
			if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) 
			{
				
				activity.selectPictures();
			}
			else
				mTextureModel.selectPicture();
			
			//Log.i(TAG, "Double Tap event.");
			return true;
		}


		@Override
		public boolean onDoubleTapEvent(MotionEvent e) 
		{
			if(doubleTapListener != null) 
			{
				return doubleTapListener.onDoubleTapEvent(e);
			}
			return true;
		}
	}
	
	

}