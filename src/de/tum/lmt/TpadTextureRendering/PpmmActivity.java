package de.tum.lmt.TpadTextureRendering;


import de.tum.lmt.textureRendering.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;





/** Description of Class 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class PpmmActivity extends Activity implements OnTouchEventListener
{

	protected static final String TAG = "PPMM";
	/*
	 *  class members
	 */
	public PPMM_FileIO mFileManager = 				null;

	
	
	// these variables manage the finger interaction modes with the device
	public GestureDetector mFingerGesturesDetector;
	public GestureDetector.SimpleOnGestureListener mFingerGesturesListener = null;
	private GestureDetector.OnDoubleTapListener doubleTapListener = null;
	
	public VelocityTracker mVelTracker;	
	public int mCurrentInteractionMode = 1;
	public static float vy, vx;
	public static float px, py;
	public static float unscaledPx, unscaledPy;
	public double mCurrentVelocity = -1.0f;	
	
	


	
	/**
	 * 
	 * @param
	 * @return
	 *  Main Touch even method. Gets called whenever Android reports a new touch
	 * event.
	 * 
	 */
	public boolean onTouchEvent(MotionEvent event) 
	{

		
		
		mFingerGesturesDetector.onTouchEvent(event);		
		
		// Set the current interaction state according to the number of fingers in contact
		// avoid enums in android, they require as twice as much memory as static ints
		switch(event.getPointerCount())
		{
		case 1:
			mCurrentInteractionMode = 1;
			break;
		case 2:
			mCurrentInteractionMode = 2;
			break;
		case 3:
			mCurrentInteractionMode = 3;
			break;
			
		}
		
		px = event.getX(); // * hapticDisplay.mHapticScaleFactorOriginal;
		py = event.getY(); // * hapticDisplay.mHapticScaleFactorOriginal;

		unscaledPx = event.getX();// * imageDisplay.mDisplayScaleFactor;
		unscaledPy = event.getY();// * imageDisplay.mDisplayScaleFactor;

		// Switch by which type of event occured
		switch (event.getActionMasked()) 
		{

			// Case where user first touches down on the screen
			case MotionEvent.ACTION_DOWN:
	
				// Reset velocities to zero
				vx = 0;
				vy = 0;
	
				// Start a new velocity tracker
				if (mVelTracker == null) 
				{
					mVelTracker = VelocityTracker.obtain();
				} 
				else 
				{
					mVelTracker.clear();
				}
	
				// Add first event to tracker
				mVelTracker.addMovement(event);
	
				onFingerDownEvent(vx, vy, px, py);
				// add further calls on implemented fingerDownEvents in other classes
				//imageDisplay.onFingerDownEvent(vx, vy, unscaledPx, unscaledPy);
	
				// Only if a single finger is in contact
				if(mCurrentInteractionMode == 1) 
				{
					
					//hapticDisplay.onFingerDownEvent(vx, vy, px, py);
					//soundDisplay.onFingerDownEvent(vx, vy, px, py);
					//ibrationDisplay.onFingerDownEvent(vx, vy, px, py);
				}
	
				if(mCurrentInteractionMode == 2) 
				{
					
					//soundDisplay.stop();
					//vibrationDisplay.stop();
				}
			
				break;
	
			case MotionEvent.ACTION_MOVE:
	
	
				// Add new motion even to the velocity tracker
				mVelTracker.addMovement(event);
	
				// Compute velocity in pixels/ms
				mVelTracker.computeCurrentVelocity(1);
	
				// Get computed velocities, and scale them appropriately
				vx = mVelTracker.getXVelocity(); // * hapticDisplay.mHapticScaleFactorOriginal;
				vy = mVelTracker.getYVelocity(); // * hapticDisplay.mHapticScaleFactorOriginal;
	
				//Log.i(TAG,String.valueOf(this.mIsMultiTouchActive));
	
				onFingerMoveEvent(vx, vy, px, py);	 
				//imageDisplay.onFingerMoveEvent(vx, vy, unscaledPx, unscaledPy);
				
				// Find out the pixel size of the current view
				//Log.i(TAG,"Width: " + this.getView().getMeasuredWidth() + px + " " + py);
				

				
				
				if(mCurrentInteractionMode == 2 ) 
				{
					//soundDisplay.onFingerMoveEvent(vx, vy, px, py);
					//vibrationDisplay.onFingerMoveEvent(vx, vy, px, py);
					//hapticDisplay.onFingerMoveEvent(vx, vy, px, py);
					//soundDisplay.stop();
					//vibrationDisplay.stop();
				}

	
				break;
	
				
				
			case MotionEvent.ACTION_UP:
				onFingerUpEvent(vx, vy, px, py);
				//imageDisplay.onFingerUpEvent(vx, vy, unscaledPx, unscaledPy);
				//hapticDisplay.onFingerUpEvent(vx, vy, px, py);
				
				if(mCurrentInteractionMode == 1) 
				{
					//soundDisplay.onFingerUpEvent(vx, vy, px, py);
					//vibrationDisplay.onFingerUpEvent(vx, vy, px, py);
				}
			
	
				break;
	
				
				
			case MotionEvent.ACTION_CANCEL:
				// Recycle velocity tracker on a cancel event
				mVelTracker.recycle();
				//set to null after recycle to prevent illegal state
				mVelTracker = null;
				//mTpad.turnOff();
				break;
			}
	
			return true;
	}


	@Override
	public void onFingerDownEvent(double vx, double vy, double px, double py)
	{
		//double velocity = Math.sqrt(vTracker.getXVelocity()*vTracker.getXVelocity() + vTracker.getYVelocity()*vTracker.getYVelocity());
	}

	@Override
	public void onFingerMoveEvent(double vx, double vy, double px, double py)
	{
		mCurrentVelocity = Math.sqrt(vx*vx + vy*vy);	
	}

	@Override
	public void onFingerUpEvent(double vx, double vy, double px, double py)
	{
		mCurrentVelocity = 0;
	}
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	public void gotoAnotherActivity(View v,int mode)
	{
		
		Intent intent = null;
		if(mode == 1)
		{
			intent = new Intent(this, SecondActivity.class);
		}
		
		if(intent != null)
			startActivityForResult(intent,mode);
		else
			return;
	}
	
	

	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		// set own class instances
	
		//mFingerGestures = new GestureDetector(this, new PPMM_FingerGestures());
		
		mFingerGesturesListener = new GestureDetector.SimpleOnGestureListener()
		{

			@Override
			public void onLongPress(MotionEvent e)
			{
				//TextureRendererActivity activity = (TextureRendererActivity) mContext;
				//activity.gotoParametersActivity(mTextureModel.getView());
				Log.i(TAG,"Long Pressed.");
				
				gotoAnotherActivity(null,1);
				
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
			{
				Log.i(TAG,"Flinged.");
				return true; 
			}

			@Override
			public boolean onScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) 
			{

				Log.i(TAG,"Two finger Scroll");
				return true;

			}

			@Override
			public boolean onDoubleTap(MotionEvent e) 
			{

				//TextureRendererActivity activity = (TextureRendererActivity) mContext;
				
				/*
				if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) 
				{
					
					activity.selectPictures();
				}
				else
					mTextureModel.selectPicture();
				*/
				Log.i(TAG, "Double tapped.");
				return true;
			}


			@Override
			public boolean onDoubleTapEvent(MotionEvent e) 
			{
				if(doubleTapListener != null) 
				{
					return doubleTapListener.onDoubleTapEvent(e);
				}
				return true;
			}
		};		
		mFingerGesturesDetector = new GestureDetector(this, mFingerGesturesListener);

		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		if (id == android.R.id.home)
		{
			finish();			
		}
		return super.onOptionsItemSelected(item);
	}



}
