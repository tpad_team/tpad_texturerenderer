package de.tum.lmt.TpadTextureRendering;

import java.io.Serializable;
import java.util.Vector;

import android.util.Log;
import android.widget.Toast;



/** s 
*
* The objects of this class contain the haptic features of a real world texture.
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/


public class Texture implements Serializable 
{

	private static final long serialVersionUID 						= 1L;
	public String mTextureName 										= null;
	public String mTextureDisplayImageFilePath 						= null;
	public String mTextureHapticImageFilePath 						= null;
	public String mSoundPath 								= null;
	public String mImpactSoundPath = null;
	public String mTextureDisplayName = null;
	
	private static final String IMAGE_PATH = ConstantsClass.folderDisplayImage;
	private static final String ROUGHNESS_PATH = ConstantsClass.folderMacroImage;
	private static final String ROUGHNESS_PREFIX = ConstantsClass.ROUGHNESS_PREFIX;
	private static final String SOUND_SUFFIX = ConstantsClass.SOUND_SUFFIX;
	private static final String SOUND_SUFFIX_IMPACT= ConstantsClass.SOUND_SUFFIX_IMPACT;
	private static final String HAPTIC_IMAGE_SUFFIX= ConstantsClass.HAPTIC_IMAGE_SUFFIX;
	private static final String SOUND_PATH = ConstantsClass.folderSoundData;
	
	
	

	
	float mWeightFric;
	float mWeightMicroRough;
	float mWeightMacroRough;
	float mAmplFric;
	float mAmplMicroRough;
	float mMicroRoughDistribution;
	float mAmplMacroRough;
	
	float mAmplHardness;
	long  mDurationHardness;
	boolean mIsSoft;
	boolean mPlaySound;
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * Empty Constructor, set dummy values.
	 * 
	 */
	public Texture()
	{
		mTextureDisplayName = "";
		mWeightFric = 0.0f;
		mWeightMicroRough = 0.0f;
		mWeightMacroRough = 1.0f;
		mAmplFric = 1.0f;
		mAmplMicroRough = 0.0f;
		mAmplMacroRough = 1.0f;
		mMicroRoughDistribution = 0.5f;	
		mAmplHardness = 0.3f;
		mDurationHardness = 10L;
		mIsSoft = false;
		mPlaySound = true;
	
	};
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * Constructor, set texture values.
	 * 
	 */
	public Texture(String name,float weightFric, float weightMicroRough, float weightMacroRough,float amplFric,float amplMicroRough, float microRoughDistribution,float amplMacroRough, float ampHardness, long durationHardness, boolean isSoft, String textureName)
	{
		mTextureName 				= name;
		mWeightFric 				= weightFric;
		mWeightMicroRough 			= weightMicroRough;
		mWeightMacroRough 			= weightMacroRough;
		mAmplFric 					= amplFric;
		mAmplMicroRough 			= amplMicroRough;
		mMicroRoughDistribution 	= microRoughDistribution;
		mAmplMacroRough 			= amplMacroRough;	
		mAmplHardness       		= ampHardness;
		mDurationHardness   		= durationHardness;
		mIsSoft 					= isSoft;
		mTextureDisplayName 		= textureName;
		mPlaySound = true;
	}
	
	
	/**
	 * Create the full path to a movement sound file.
	 * @param
	 * @return String
	 * 
	 * 
	 */	
	String createSoundName() 
	{
		if(mSoundPath != null) 
		{
			return mSoundPath;
		}
		else 
		{
			if (mTextureName == null) 
			{
				Log.e("Texture", "Texture name is null");
				return "";
			}
			else
			{
				return SOUND_PATH + mTextureName.concat(SOUND_SUFFIX);
			}
		}
		
	}
	
	/**
	 * Create the full path to an impact sound file.
	 * @param
	 * @return String
	 * 
	 */
	String createImpactSoundName() 
	{
		if(mImpactSoundPath != null)
		{
			return mImpactSoundPath; 
		}
		else 
		{
			return SOUND_PATH + mTextureName.concat(SOUND_SUFFIX_IMPACT);
		}
	}
	
	/**
	 * Create the full path to a haptic bitmap.
	 * @param
	 * @return String
	 * 
	 * 
	 */	
	String createHapticBitmapName() 
	{	
		if(mTextureHapticImageFilePath != null)
			return mTextureHapticImageFilePath;
		else
			return ROUGHNESS_PATH + ROUGHNESS_PREFIX + mTextureName + HAPTIC_IMAGE_SUFFIX;
	}
	
	
	/**
	 * Create the full path to a display bitmap.
	 * @param
	 * @return String
	 * 
	 * 
	 */	
	String createImageBitmapName() 
	{
	
		if(mTextureDisplayImageFilePath!=null)
			return mTextureDisplayImageFilePath;
		else
			return IMAGE_PATH + mTextureName + HAPTIC_IMAGE_SUFFIX;
	}

	
	/**
	 * Getter for texture name.
	 * @param
	 * @return String
	 */
	String getTextureID() 
	{
		return this.mTextureName;
	}
	
	
}
