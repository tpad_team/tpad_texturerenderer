package de.tum.lmt.TpadTextureRendering;

import java.util.Iterator;
import java.util.Vector;


/** 
*
*
*
*  This class constitutes a database for various objects of the Texture class
*  It uses prestored textures, however it also can add new textures received via RemoteCommunicatin
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/

public class TextureDB
{
	public Vector<Texture> mTextures;
	
	
	public final int numOfParameters = 9;
	public int currentLoadedTextureNumber = -1;
	
	public TextureDB()
	{
		mTextures = new Vector<Texture>();


		//                                                               ///Weights ////////  Amplitudes,Distr /////////Hardness and Duration (Vibrator)
		// 													       Fric Micro Macro     Fric  Micro  MicroDist Macro           Hardn  Dura
		mTextures.add(new Texture("00_tpaddemo",					0.0f,0.0f,1.0f, 	1.0f,  1.0f,0.5f  		,1.0f,   		0.8f, 100L,      false    , "TPad Demo"));	
		mTextures.add(new Texture("00_chessdemo",					0.0f,0.0f,1.0f, 	1.0f,  1.0f,0.5f  		,1.0f,   		0.8f, 100L,      false    , "TPad Chess Demo"));	
		mTextures.add(new Texture("01_carpet",						0.0f,0.5f,0.5f, 	1.0f,  1.0f,0.4f  		,1.0f,   		0.5f, 100L,      false    , "Carpet"));
		mTextures.add(new Texture("02_steelwool",					0.0f,0.6f,0.4f, 	1.0f,  1.0f,0.5f  		,1.0f,   		0.3f, 100L,      true     , "Steel Wool" ));
		mTextures.add(new Texture("03_cork",						0.3f,0.3f,0.4f, 	0.2f,  1.0f,0.5f  		,1.0f,   		0.8f, 120L,      false    , "Cork"));	
		mTextures.add(new Texture("04_stone",						0.1f,0.4f,0.5f, 	1.0f,  1.0f,0.4f  		,1.0f,   		1.0f, 150L,     false    , "Stone Tile" ));
		mTextures.add(new Texture("05_marble",						0.5f,0.4f,0.1f, 	0.4f,  0.6f,0.2f  		,0.4f,   		1.0f, 200L,     false    , "Marble"));
		mTextures.add(new Texture("06_styrofoam",					0.1f,0.4f,0.5f, 	1.0f,  0.7f,0.3f  		,1.0f,   		0.8f, 100L,      false    , "Styro Foam" ));
		mTextures.add(new Texture("07_coarsefoam",					0.0f,0.0f,1.0f, 	1.0f,  1.0f,0.5f  		,1.0f,   		0.5f, 100L,      true     , "Coarse Foam" ));
		mTextures.add(new Texture("08_textile",						0.5f,0.3f,0.2f, 	0.9f,  0.8f,0.5f  		,1.0f,   		0.5f, 100L,      false    , "Textile" ));
		mTextures.add(new Texture("09_rubberprofile",				0.0f,0.3f,0.7f, 	1.0f,  0.5f,0.5f  		,1.0f,   		0.5f, 100L,      true     , "Rubber"));
		mTextures.add(new Texture("10_brass",						0.8f,0.1f,0.1f, 	1.0f,  1.0f,0.5f  		,1.0f,   		1.0f, 200L,      false    , "Brass"));
		mTextures.add(new Texture("11_hardfoam",					0.3f,0.3f,0.4f, 	1.0f,  0.8f,0.2f  		,0.6f,   		0.5f, 100L,      false    , "Hard foam" ));
		mTextures.add(new Texture("12_crushedrock",					0.0f,0.5f,0.5f, 	1.0f,  1.0f,0.6f  		,1.0f,   		0.6f, 110L,      false    , "Crushed Stone"));
		mTextures.add(new Texture("13_aluminummesh",				0.0f,0.0f,1.0f, 	1.0f,  1.0f,0.5f  		,1.0f,   		0.5f, 120L,      false    , "Aluminum Mesh"));
		mTextures.add(new Texture("14_finefiber",					0.3f,0.3f,0.4f, 	1.0f,  0.8f,0.2f  		,0.6f,   		0.2f, 100L,      false    , "Fine Fiber" ));
		mTextures.add(new Texture("15_smoothcork",					0.3f,0.0f,0.7f, 	1.0f,  0.6f,0.2f  		,0.6f,   		0.8f, 110L,     false    , "Smooth cork"));
		mTextures.add(new Texture("16_textile",						0.3f,0.0f,0.7f, 	1.0f,  0.8f,0.2f  		,0.6f,   		0.3f, 100L,      false    , "Textile" ));	
		mTextures.add(new Texture("17_wood",						0.2f,0.2f,0.6f,		1.0f,  0.8f,0.7f  		,1.0f,   		0.8f, 150L,      false    , "Wood"));
		mTextures.add(new Texture("18_woodprofile",					0.0f,0.1f,0.9f, 	1.0f,  1.0f,0.5f  		,1.0f,   		1.0f, 150L,     false    , "Wooden Profile"));
		mTextures.add(new Texture("19_woodprofile",					0.0f,0.1f,0.9f, 	1.0f,  1.0f,0.5f  		,1.0f,   		1.0f, 150L,     false    , "Wooden Profile"));
		mTextures.add(new Texture("20_textile",						0.3f,0.1f,0.7f, 	1.0f,  0.8f,0.2f  		,1.0f,   		0.3f, 100L,      false    , "Textile" ));	
		mTextures.add(new Texture("22_wood",						0.1f,0.1f,0.8f,		0.2f,  1.0f,0.5f  		,1.0f,   		0.8f, 150L,      false    , "Wood"));

	
	
	}
	/**
	 * Add a new texture to the database with all its parameters necessary for haptic/visible/audio rendering.
	 * @param
	 * @return
	 * 
	 */
	public void addTextureToDB(String textureName,String name,float weightFric, float weightMicroRough, float weightMacroRough,float amplFric,float amplMicroRough,float microRoughDistribution,float amplMacroRough,float ampHardness, long durationHardness, boolean isSoft, String textureDisplayName)
	{
		mTextures.add(new Texture(textureName,weightFric,weightMicroRough, weightMacroRough,amplFric, amplMicroRough,microRoughDistribution, amplMacroRough, ampHardness,durationHardness, isSoft,textureDisplayName));		
	}
	public void addTextureToDB(Texture tex)
	{
		mTextures.add(tex);
	}
	
	public void removeTexture(String texturName)
	{
		// To Do
	}
	/**
	 * Retrieve a texture from the database explicitly by name.
	 * @param name string
	 * @return
	 * 
	 */
	public Texture getTextureByName(String name)
	{
		Texture searchedTexture = null; // new Texture();
		Texture tmp;

		int i = 0;
		
		Iterator<Texture> itr = mTextures.iterator();
		while(itr.hasNext())
		{
			tmp = itr.next();
			if(name.equals(tmp.mTextureName))
			{
				searchedTexture = tmp;
				currentLoadedTextureNumber = i;
			}
			i++;

		}
		return searchedTexture;
	}
	/**
	 * Retrieve a texture entry number from the database explicitly by name.
	 * @param name string
	 * @return
	 * 
	 */
	public int getTextureNumber(String name)
	{
		Texture tmp;

		int i = 0;
		
		Iterator<Texture> itr = mTextures.iterator();
		while(itr.hasNext())
		{
			tmp = itr.next();
			if(name.equals(tmp.mTextureName))
			{
				return i+1;
			}
			i++;

		}
		return -1;	
	}
	public Texture getTextureByNumber(int number) 
	{
		
		if(number >=0 && number < this.mTextures.size()) 
		{
			this.currentLoadedTextureNumber = number;
			return this.mTextures.get(number);
		}
		else 
		{
			return null;
		}
		
	}
	

}