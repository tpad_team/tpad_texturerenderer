package de.tum.lmt.TpadTextureRendering;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.content.ContentResolver;
import android.content.Context;



/** Utility Class 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public final class TextureUtility 
{

	private static void method()
	{
		
	}

	/**
	 * Function to remove file extensions
	 * @param file path string with extension
	 * @return file path string without extension
	 */
	public static String removeExtension(String s) 
	{

		String separator = System.getProperty("file.separator");
		String filename;

		// Remove the path upto the filename.
		int lastSeparatorIndex = s.lastIndexOf(separator);
		if (lastSeparatorIndex == -1) 
		{
			filename = s;
		} 
		else 
		{
			filename = s.substring(lastSeparatorIndex + 1);
		}

		// Remove the extension.
		int extensionIndex = filename.lastIndexOf(".");
		if (extensionIndex == -1)
			return filename;

		return filename.substring(0, extensionIndex);
	}
		
	
	/**
	 * This function gets a URI to a display bitmap, from which the main texture ID string is extracted. The underlying URI scheme in this case is "content". 
	 * @param uri
	 * @return
	 */
	public static String getFileName(Uri uri, Context context)
	{
		String result = null;
		if (uri.getScheme().equals("content")) 
		{
			Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
			try 
			{
				if (cursor != null && cursor.moveToFirst()) 
				{
					result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
				}
			} 
			finally 
			{
				cursor.close();
			}
		}
		if (result == null) 
		{
			result = uri.getPath();
			int cut = result.lastIndexOf('/');
			if (cut != -1) 
			{
				result = result.substring(cut + 1);
			}
		}
		return result;
	}	
}
