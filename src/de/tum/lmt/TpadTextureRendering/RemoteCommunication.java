package de.tum.lmt.TpadTextureRendering;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import de.tum.lmt.textureRendering.R;
import de.tum.lmt.textureRendering.R.drawable;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.FileObserver;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


/** You can use this class to handle incoming bluetooth transmissions of externally recorded textures (app: textureRecognizer)
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class RemoteCommunication 
{
	private String TAG = "RemoteCommunications";	
	
	TextureModel mTexModel 															= null;
	FileObserver mFileObserverBluetooth 											= null;
	private static final String mBluetoothPath 										= ConstantsClass.bluetoothPath;
	public int mCounter 															= 0;
	File mParametersFile 															= null;
	String mParametersFileName 														= null;
	Uri mMacroDisplayUri 															= null;
	String mMacroDisplayPath 														= null;
	Uri mDisplayUri 																= null;
	File mImpactSound 																= null;
	File mMovementSound 															= null;

	File mTmpDisplayFile 															= null;
	File mTmpMacroFile 																= null;
	File mTmpSoundFile 																= null;
	File mTmpParametersFile 														= null;

	String mNewTextureName 															= "";
	String mReadFromParametersFile 													= "";
	String mDisplayBitmapPath 														= "";
	String mMacroBitmapPath 														= "";
	String mImpactPath																= "";
	String mSoundPath 																= "";
	String mParamtersPath 															= "";
	String newFile 																	= "";
	Set<String> mDetectedFiles 														= new HashSet<String>(); // member




	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	public RemoteCommunication(TextureModel texModel)
	{
		mTexModel = texModel;
		
	}

	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	boolean parseMacroFile(String file) 
	{
		mMacroDisplayPath = mBluetoothPath + file; 
		mTmpMacroFile     = new File(mMacroDisplayPath);
		mMacroDisplayUri  = Uri.fromFile(new File(mMacroDisplayPath));

		if(mTmpMacroFile != null)
		{
			if( (mTmpMacroFile.length() > 0)  )
			{
				mMacroDisplayPath = mBluetoothPath + file;
				Log.i(TAG, "Successfully read " + String.valueOf(mTmpMacroFile.length() + " bytes from file " + mBluetoothPath + file));
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}

	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	boolean parseDisplayFile(String file) 
	{
		mDisplayBitmapPath = mBluetoothPath + file;
		mTmpDisplayFile    = new File(mDisplayBitmapPath);
		mDisplayUri        = Uri.fromFile(new File(mDisplayBitmapPath));

		if(mTmpDisplayFile != null)
		{
			if( (mTmpDisplayFile.length() > 0)  )
			{
				mNewTextureName = file;
				Log.i(TAG, "Successfully read " + String.valueOf(mTmpDisplayFile.length() + " bytes from file " + mBluetoothPath + file));
				return true;
			}
			else 
			{
				//Log.e(TAG, "" + String.valueOf(mTmpDisplayFile.length() + " bytes from file " + mBluetoothPath + file));
				return false;
			}
		}
		else 
		{
			return false;
		}
	}

	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * taken from http://stackoverflow.com/questions/12910503/read-file-as-string
	 * @param filePath
	 * @return
	 */
	static String readFileAsString(String filePath) 
	{
		String result = "";
		File file = new File(filePath);
		if ( file.exists() )
		{
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);
				char current;
				while (fis.available() > 0)
				{
					current = (char) fis.read();
					result = result + String.valueOf(current);
				}
			} catch (Exception e)
			{
				Log.d("TourGuide", e.toString());
			} 
			finally 
			{
				if (fis != null)
					try 
					{
						fis.close();
					} 
				catch (IOException ignored) 
				{
				}
			}
		}
		return result;
	}

	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	Texture readJson(String filename) 
	{
		Log.i(TAG,  "reading json");

		Texture newTexture = new Texture();
		newTexture.mTextureName 			= "display";		

		String name = readFileAsString(filename);
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(name);

			newTexture.mAmplMacroRough = (float) jsonObject.getDouble("AmplitudeMacroRoughness");
			newTexture.mAmplHardness = (float) jsonObject.getDouble("hardness");
			newTexture.mAmplFric = (float) jsonObject.getDouble("AmplitudeFriction");
			newTexture.mAmplMicroRough = (float) jsonObject.getDouble("AmplitudeMicroRoughness");
			newTexture.mDurationHardness = (long) jsonObject.getDouble("DurationHardness");
			newTexture.mMicroRoughDistribution = (float) jsonObject.getDouble("DistributionMicroRoughness");
			newTexture.mWeightMacroRough  		= (float) jsonObject.getDouble("WeightMacroRoughness");
			newTexture.mWeightMicroRough  		= (float) jsonObject.getDouble("WeightMicroRoughness");
			newTexture.mWeightFric        		= (float) jsonObject.getDouble("WeightFriction");
			newTexture.mAmplHardness      		= (float) jsonObject.getDouble("AmplitudeHardness");

			Log.i(TAG, "Successfully loaded json file");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newTexture;
	} 


	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	boolean parseParametersFile(String file) 
	{

		String ret = "";
		FileInputStream fos;

		mParamtersPath = mBluetoothPath + file;
		mTmpParametersFile = new File(mParamtersPath);

		if(mTmpParametersFile != null)
		{
			if(mTmpParametersFile.length() == 0)
			{		
				//Log.i(TAG, "Reading from " + mTmpParametersFile.getAbsoluteFile() + ", length " + mTmpParametersFile.length());
				return false;
			}

			try
			{
				fos = new FileInputStream(mTmpParametersFile);

				InputStreamReader isr = new InputStreamReader ( fos ) ;
				BufferedReader buffreader = new BufferedReader ( isr ) ;

				String readString = buffreader.readLine ( ) ;
				while ( readString != null ) 
				{
					ret = ret + readString;
					readString = buffreader.readLine ( ) ;
				}
				isr.close();
				fos.close();
			}
			catch(IOException ioex){}

			Log.i(TAG, "Successfully read " + String.valueOf(mTmpParametersFile.length() + " bytes from file " + mBluetoothPath + file));

			mReadFromParametersFile = ret;

			return true;
		}
		else
			return false;

	}

	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */
	boolean parseSoundFile(String file) 
	{	
		mSoundPath = mBluetoothPath + file;
		mMovementSound = new File(mSoundPath);

		if(mMovementSound != null)
		{
			if(mMovementSound.length() > 0)
			{
				Log.i(TAG, "Successfully read " + String.valueOf(mMovementSound.length() + " bytes from file " + mBluetoothPath + file));
				return true;
			}
			else
			{
				return false;
			}
		}
		else 
		{
			Log.e(TAG,"Error when reading file " + mBluetoothPath + file);
			return false;
		}
	}
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */	
	boolean parseImpactFile(String file) 
	{	
		mImpactPath = mBluetoothPath + file;
		mImpactSound = new File(mImpactPath);

		if(mImpactSound != null)
		{
			if(mImpactSound.length() > 0)
			{
				Log.i(TAG, "Successfully read " + String.valueOf(mImpactSound.length() + " bytes from file " + mBluetoothPath + file));
				return true;
			}
			else
			{
				return false;
			}
		}
		else 
		{
			Log.e(TAG,"Error when reading file " + mBluetoothPath + file);
			return false;
		}
	}	




	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	boolean parseFileBluetooth(String file)
	{
		if( file.contains("macro") ) 
		{
			return parseMacroFile(file);
		}
		else 
		{
			if (file.contains("display")) 
			{
				return parseDisplayFile(file);
			}
			else 
			{
				if (file.contains("sound")) 
				{
					return parseSoundFile(file);
				}
				else 
				{
					if (file.contains("parameters"))
					{
						return parseParametersFile(file);
					}
					else
					{
						if (file.contains("impact")) 
						{
							
							return parseImpactFile(file);
						}
						else
						{
							Log.e(TAG, "Some unknown file arrived " + file);
							return false;

						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	public void StartObservingBluetooth() 
	{

		mFileObserverBluetooth = new FileObserver(mBluetoothPath) 
		{
			@Override
			public void onEvent(int event1, String file1) 
			{
				if(event1 == FileObserver.CLOSE_WRITE)
				{

					if (mDetectedFiles.contains(file1)) 
					{
						//Log.i(TAG, file1 + " already inside");
						//SystemClock.sleep(3000); // TODO why do we need this?
						parseFileBluetooth(file1); // TODO why do we need this? if the file has already been added, it should also have been parsed
					}
					else 
					{
						newFile = file1;
						
						//SystemClock.sleep(3000);
						if(parseFileBluetooth(file1)) 
						{
							mDetectedFiles.add(file1);
						}

					}
				}
			}
		};
		mFileObserverBluetooth.startWatching();
	}


	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */
	public void CheckForNewReceivedFiles(Context context)
	{

		if (mDetectedFiles.size() == 1 && this.mTexModel != null) {
			this.mTexModel.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		}
		
		//Log.i(TAG,"Check for new file ..." + mDetectedFiles.size());
		
		if (newFile.compareTo("") != 0) 
		{
			
			Log.i(TAG,"New file arrived. Current counter: " + mDetectedFiles.size());
		
			newFile = "";
			if (mDetectedFiles.size() == 5)  
			{
				Log.i(TAG, "Complete texture model received (5 files)");

				
				
				// todo 07072016 disable wav for received textures over bluetooth
				this.mTexModel.hapticDisplay.mUseWav = false;
				
				sendNotification(context,mDisplayBitmapPath,mMacroDisplayPath,mSoundPath,mImpactPath,mReadFromParametersFile);
		
				Toast.makeText(context,"Texture model received. Check your notifications!", Toast.LENGTH_LONG).show();
				
				// Enable screen orientation again:
				this.mTexModel.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	
				
				

				mDetectedFiles.clear();
			}
		}		
	}



	public void stopCommunication()
	{
		mFileObserverBluetooth.stopWatching();
	}
	
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
     * Send a sample notification using the NotificationCompat API.
     */
    public void sendNotification(Context context, String fileImageDisplay, String fileImageHaptic, String fileSoundMove, String fileSoundImpact, String fileParameters) {
 
        /** Create an intent that will be fired when the user clicks the notification.
         * The intent needs to be packaged into a {@link android.app.PendingIntent} so that the
         * notification service can fire it on our behalf.
         */
        Intent intent = new Intent(context, TextureRendererActivity.class);
        intent.putExtra("fileImageDisplay", fileImageDisplay);
        intent.putExtra("fileImageHaptic", fileImageHaptic);
        intent.putExtra("fileSoundMove", fileSoundMove);
        intent.putExtra("fileSoundImpact", fileSoundImpact);
        intent.putExtra("fileParameters", fileParameters);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
 
        
        
        /**
         * Use NotificationCompat.Builder to set up our notification.
         */
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
 
        /** Set the icon that will appear in the notification bar. This icon also appears
         * in the lower right hand corner of the notification itself.
         *
         * Important note: although you can use any drawable as the small icon, Android
         * design guidelines state that the icon should be simple and monochrome. Full-color
         * bitmaps or busy images don't render well on smaller screens and can end up
         * confusing the user.
         */
        builder.setSmallIcon(R.drawable.ic_texturerenderer);
 
        // Set the intent that will fire when the user taps the notification.
        builder.setContentIntent(pendingIntent);
 
        // Set the notification to auto-cancel. This means that the notification will disappear
        // after the user taps it, rather than remaining until it's explicitly dismissed.
        builder.setAutoCancel(false);
 
        /**
         *Build the notification's appearance.
         * Set the large icon, which appears on the left of the notification. In this
         * sample we'll set the large icon to be the same as our app icon. The app icon is a
         * reasonable default if you don't have anything more compelling to use as an icon.
         */
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
 
        builder.setContentTitle("New Texture Received");
        builder.setContentText("You have received a new texture to display.");
        builder.setSubText("Tap to open the texture in the texture renderer");
        builder.setTicker("New texture message fully received!");
        
        builder.setPriority(Notification.PRIORITY_HIGH);
        
        long[] pattern = new long[] {10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10};
        builder.setVibrate(pattern);

 
 
        /**
         * Send the notification. This will immediately display the notification icon in the
         * notification bar.
         */
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
        		context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

}
