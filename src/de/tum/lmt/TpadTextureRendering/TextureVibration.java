package de.tum.lmt.TpadTextureRendering;

import ioio.lib.spi.Log;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Toast;




/** This class is supposed to control the internal vibration actuator and to react accordingly to users touch events. 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/


public class TextureVibration implements OnTouchEventListener 
{

	private final String TAG 				= "TextureVibration";
	
	private TextureModel mTextureModel 		= null;
	private Texture mCurrentTexture			= null;
	public  Context mContext 				= null;
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * Main Constructor, set context and according texture model.
	 * 
	 */
	public TextureVibration(Context context, TextureModel texModel,AttributeSet attrs) 
	{
		mContext = context;
		mTextureModel = texModel;
	}
	public void setTexture(Texture newTexture)
	{
		this.mCurrentTexture = newTexture;
	}

	
	
	/**
	 * Play a specific vibration pattern for the initial finger contact with the screen that is supposed to recreate the initial impact that we feel while touching a surface.
	 * @param
	 * @return
	 * 
	 * 
	 */
	public void onFingerDownEvent(double vx, double vy, double px, double py) 
	{
		// impact simulation
		if (mCurrentTexture == null) 
		{
			Log.e(TAG,  "mCurrentTexture == null");
		}
		else 
		{
			GenerateHardnessEffect(mCurrentTexture.mAmplHardness,mCurrentTexture.mDurationHardness);
		}
	}


	public void onFingerMoveEvent(double vx, double vy, double px, double py)
	{
		
	}

	public void onFingerUpEvent(double vx, double vy, double px, double py)
	{

		 try 
		 { 
			 mTextureModel.mVibrator.cancel();
		 }
		 catch (NullPointerException ex) 
		 {  
			 Log.e(TAG, "Vibrator is not initialized."); 
		 }
		
	}
		
	
	
	
	
	/**
	 * Vibrate specifically according to given length and strength.
	 * @param strength and length of the pattern to be displayed
	 * @return
	 */
	public void GenerateHardnessEffect(float amplitude,long decayTime)
	{
		final float extendedEffectMultiplicator = 0.8f;
		long[] pattern1 = genVibratorPatternPWM(amplitude,decayTime);
		long[] pattern2 = genVibratorPatternPWM(extendedEffectMultiplicator * amplitude, decayTime);

		
		try 
		{
			mTextureModel.mVibrator.vibrate(pattern1,-1);
			try {
				Thread.sleep(40);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mTextureModel.mVibrator.vibrate(pattern2,-1);

		}
		catch (NullPointerException ex) 
		{  
			Log.e(TAG, "Vibrator is not initialized.");
		}
		

	}
	

	/**
	 * PWM like generation of a vibration pattern.
	 * @param intensity and duration
	 * @return
	 */
	
	public long[] genVibratorPatternPWM( float intensity, long duration )
	{
	
		if(intensity > 1.0f) 
		{
			intensity = 1.0f;
		}
		else if(intensity < 0.0f)
		{
			intensity = 0.0f;
		}	
		
		final int cycleLength = 20;
		
		int numberOfRepetitions = (int)(duration/cycleLength);
		int remainingValues     = (int)(duration%cycleLength);
		
		long[] pattern = new long[ numberOfRepetitions*cycleLength ];
		
		
		for(int i=0;i<numberOfRepetitions;i++)
		{
			int numberOfHighValues = (int)(intensity*cycleLength);
			int numberOfLowValues  = cycleLength-numberOfHighValues;
			for(int j = 0;j <  numberOfLowValues;j++)  pattern[(i*cycleLength) + j ] = 0L;
			for(int j = 0;j < numberOfHighValues;j++)  pattern[(i*cycleLength) + (j+numberOfLowValues) ] = 1L;
		}

	    return pattern;
	}	
	
	public void stop() 
	{
		mTextureModel.mVibrator.cancel();
	}
}
