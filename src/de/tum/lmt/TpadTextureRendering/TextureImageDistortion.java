package de.tum.lmt.TpadTextureRendering;

import ioio.lib.spi.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import android.graphics.Bitmap;



/** Description of Class 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class TextureImageDistortion 
{
	// distortion stuff
	private boolean mInitialized = false;
	private boolean mLoadedBitmap = false;
	
	private double mDistortionK = ConstantsClass.DISTORTION_K;
	private int mNeighborhoodDistortion = ConstantsClass.NEIGHBORHOOD_SIZE_TO_DISTORT;
	private long mTimeoutLastTouchms = ConstantsClass.TIMEOUT_LAST_TOUCH_MS;
	private int mFingerRegionUnchanged = ConstantsClass.NEIGHBORHOOD_SIZE_UNCHANGED_DISTORT;
	public float mDisplayScaleFactor = 1.0f;
	
	WaveReader reader;
	
	
	private ByteBuffer handlerOriginal = null;
	private ByteBuffer handlerDistorted = null;
	private boolean mSmoothBoundary = true;
	private long mLastDistortTS;

	private String TAG = "TextureImageDistortion";

	private Bitmap mLocalBitmap;
	TextureImage mTextureImage;

	private native ByteBuffer jniStoreBitmapData(Bitmap bitmap);
	private native void jniDistortBitmap(ByteBuffer handler1, ByteBuffer handler2, int centerX, int centerY, double k, int neighborhood, 
			int scaledFingerUnchangedSize, int width, int height, int min_index, int max_index);
	private native Bitmap jniGetBitmapFromStoredBitmapData(ByteBuffer handler);
	private native void jniFreeBitmapData(ByteBuffer handler);
	
	private native void jniGiveInputToFeatureComputation(Bitmap bitmapFlash, Bitmap bitmapNoFlash, double[] array, int size);

	class BitmapChangedIndiceRange {
		public int min_index, max_index;
		public void initFromBoundary(RegionBoundary t, int width, int height) {
			min_index = t.min_x + t.min_y*width;
			max_index = t.max_x + t.max_y*width;
		}
	}

	class RegionBoundary {
		public int min_x, max_x, min_y, max_y;
		public void initFromTouch(int centerX, int centerY, int width, int height, int scaled_neighborhood) {
			min_x = Math.max(0, centerX-scaled_neighborhood);
			max_x = Math.min(width, centerX+scaled_neighborhood);
			min_y = Math.max(0, centerY-scaled_neighborhood);
			max_y = Math.min(height, centerY+scaled_neighborhood);
		}
	}

	RegionBoundary currentBoundary;
	BitmapChangedIndiceRange current, last;
	


	Bitmap getDistortedBitmap() {
		return mLocalBitmap;
	}


	static boolean jniLoaded = false;

	static {
		try {
			System.loadLibrary("helpers");
			Log.i("TextureImage", "jni library successfully loaded, prepare to rock!");
			jniLoaded = true;
		}
		catch (UnsatisfiedLinkError e) {
			Log.i("TextureImage", "jni library not found, using java instead");
			jniLoaded = false;
		}
	}

	public void displayBitmapCalled() {
		mLoadedBitmap = true;
		mInitialized = false;//we need to reinitialize bitmap copies we have locally
	}

	public void initializeMatData(TextureImage m, Bitmap bitmapOriginal) {
		
		
		
		mTextureImage = m;
		//Log.i(TAG , "calling initializeMatData");
		if (!mInitialized) {
			if (!mLoadedBitmap) { // if no bitmap available, doing nothing
				return;
			}
			else { // initialize local copies of data

				if (handlerOriginal != null) { // clear out native handlers for bitmaps, will be reinited automatically
					jniFreeBitmapData(handlerOriginal);
					handlerOriginal = null;
				}
				if (handlerDistorted != null) {
					jniFreeBitmapData(handlerDistorted);
					handlerDistorted = null;
				}


				//Log.i(TAG, "initializeMatData " + mWidth + "x" + mHeight);
				mLocalBitmap = bitmapOriginal.copy(Bitmap.Config.ARGB_8888, true);
				mInitialized = true;
				//Log.i(TAG, "displayBitmap size" + mLocalBitmap.getHeight() + "x" + 
				//		mLocalBitmap.getWidth());// + "...." + mDisplayBitmap.getHeight());

				mLastDistortTS = 0;
				currentBoundary = new RegionBoundary();
				current = new BitmapChangedIndiceRange();
				last = new BitmapChangedIndiceRange();
				
				// test - this is for kevin only
				if (jniLoaded) {
					try {
						//getSound();
						double[] arraySound = new double[3]; int size = 3;// test data only
						//jniGiveInputToFeatureComputation(bitmapOriginal, bitmapOriginal, arraySound, size);//TODO uncomment to test stuff on native					
				
					}
					catch(NullPointerException ex) {
						Log.e(TAG, "NullPointerException encountered when initializing distortion data");
					}
				}
			}

		}
	}
	
	private void getSound() {
		Log.i(TAG, "Sound started reading");
		String name = "/sdcard/test.wav";
		 InputStream is = null;
	
		    try {
		        is = new FileInputStream(name);
	
		        //is.close(); 
		    } catch (FileNotFoundException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    }
	
		try {
			Log.i(TAG, "Sound calling wave reader");
			reader = new WaveReader(is);
			
			Log.i(TAG, "Sound finished calling wave reader, number of samples " + reader.getSize());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NullPointerException ex) {
			Log.e(TAG, "NullPointerException!");
		}
	}

	public boolean startDistortion(double vx, double vy, double px, double py, Bitmap bitmapToDistort, float visualScaleFactorInput) {
		long currentTS = System.currentTimeMillis();
		if (mInitialized && (currentTS-mLastDistortTS)>mTimeoutLastTouchms) {
			Log.i(TAG, "onFingerDownEvent " + px + "," + py);

			mLocalBitmap = bitmapToDistort.copy(Bitmap.Config.ARGB_8888, true);
			Log.i(TAG, "onFingerDownEvent finished copy");

			if (jniLoaded) {
				distortImageNative(mDistortionK, mNeighborhoodDistortion, (int)Math.round(px), (int)Math.round(py),
						bitmapToDistort, visualScaleFactorInput);// native implementation
			}
			else {
				distortImageJava(mDistortionK, mNeighborhoodDistortion, (int)Math.round(px), (int)Math.round(py),
						bitmapToDistort, visualScaleFactorInput);// java implementation
			}

			mLastDistortTS = System.currentTimeMillis();
			return true;
		}
		else {
			return false;
		}
	}

	public boolean stopDistortion(Bitmap bitmapOriginal) {
		if (mInitialized) {
			//Log.i(TAG, "onFingerUpEvent " + px + "," + py);

			mLocalBitmap = bitmapOriginal.copy(Bitmap.Config.ARGB_8888, true);
			Log.i(TAG, "onFingerUpEvent finished copy");
			return true;
		}
		else {
			return false;
		}
	}

	private void distortImageNative(double k, int neighborhood, int centerX, int centerY, Bitmap bitmapOriginal, float visualScaleFactorInput) {
		Log.i(TAG, "----BEGIN:bitmap ");
		if (handlerOriginal == null) {
			handlerOriginal = jniStoreBitmapData(bitmapOriginal);
			Log.i(TAG, "gettig local version now 1");
		}
		if (handlerDistorted == null) {
			handlerDistorted = jniStoreBitmapData(mLocalBitmap);
			Log.i(TAG, "gettig local version now 2");
		}

		float scaling_factor = 1;//visualScaleFactorInput; // TODO it turns out it is better to keep this unchanged
		int scaledFingerUnchangedSize = Math.round(mFingerRegionUnchanged / scaling_factor);
		int neighborhoodDistortionScaled = Math.round(mNeighborhoodDistortion / scaling_factor);

		if (currentBoundary != null) {
			currentBoundary.initFromTouch(centerX, centerY, bitmapOriginal.getWidth(), bitmapOriginal.getHeight(), 
					mNeighborhoodDistortion);
			current.initFromBoundary(currentBoundary, bitmapOriginal.getWidth(), bitmapOriginal.getHeight());
		}
		else {
			Log.i(TAG, "current boundary null");
		}

		Log.i(TAG, "passing indeces " + last.min_index + ".." + last.max_index);

		jniDistortBitmap(handlerOriginal, handlerDistorted, centerX, centerY, k, neighborhoodDistortionScaled, scaledFingerUnchangedSize, 
				mLocalBitmap.getWidth(), mLocalBitmap.getHeight(), last.min_index, last.max_index);

		mLocalBitmap = jniGetBitmapFromStoredBitmapData(handlerDistorted);

		last = current;
		
		Log.i(TAG, "----END:bitmap ");
		//return mLocalBitmap;
	}

	/**
	 * Distorts haptic data display based on the formula for pincushion distortion
	 * @param k - distortion coefficient (>0 for pincushion, <0 for barrel distortion)
	 * @param neighborhood - number of pixels in the vicinity to distort
	 * @param centerX - X coordinate
	 * @param centerY - Y coordinate
	 */
	private void distortImageJava(double k, int neighborhood, int centerX, int centerY, Bitmap bitmapOriginal, float visualScaleFactorInput) {

		// scale down and scale up
		float scaling_factor = 1;//visualScaleFactorInput; // TODO it turns out it is better to keep this unchanged correct 

		centerX = (int)Math.round(centerX/scaling_factor);
		centerY = (int)Math.round(centerY/scaling_factor);

		int scaledFingerUnchangedSize = Math.round(mFingerRegionUnchanged / scaling_factor);
		int scaled_neighborhood = (int)Math.round(neighborhood/scaling_factor); // TODO use appropriate scale variable

		int height = bitmapOriginal.getHeight();
		int width = bitmapOriginal.getWidth();
		int min_x = Math.max(0, centerX-scaled_neighborhood);
		int max_x = Math.min(width, centerX+scaled_neighborhood);
		int min_y = Math.max(0, centerY-scaled_neighborhood);
		int max_y = Math.min(height, centerY+scaled_neighborhood);
		// ---------------> X
		// |*****************
		// |*****************
		// |*****************
		// |*****************
		// | ****************
		// V Y***************
		int pixel;
		int xp=0, yp=0;
		double r2, theta, new_r;

		Log.i(TAG, "----BEGIN:bitmap " + width + "x" + height + "==" + min_x + ", " + max_x + ".." + min_y + ", " + max_y);

		for (int x=min_x; x<max_x; x=x+1) {
			for (int y=min_y; y<max_y; y=y+1) {
				if (Math.abs(x - centerX) > scaledFingerUnchangedSize || Math.abs(y-centerY) > scaledFingerUnchangedSize) {  
					// first transform into polar coordinate system
					double x_dist = Math.min(Math.abs(x-min_x), Math.abs(x-max_x));
					double y_dist = Math.min(Math.abs(y-min_y), Math.abs(y-max_y));
					double dist_boundary = Math.min(x_dist, y_dist);
					dist_boundary /= scaled_neighborhood;
					r2 = Math.pow(x-centerX, 2.0) + Math.pow(y-centerY, 2.0);
					theta = Math.atan2((y-centerY),(x-centerX));
					// here we actually distort the image
					if (dist_boundary < 0.3 && mSmoothBoundary) {
						new_r = Math.sqrt(r2)*(1-dist_boundary);
					}
					else {
						new_r = Math.sqrt(r2)*(1+k*r2*scaling_factor);
					}
					// transform into cartesian coordinate system
					xp = (int) Math.round(centerX+new_r*Math.cos(theta));
					yp = (int) Math.round(centerY+new_r*Math.sin(theta));

					if (xp<width && xp>=0 && yp<height && yp>=0) {

						pixel = bitmapOriginal.getPixel(xp, yp);
						mLocalBitmap.setPixel(x, y, pixel);
					}
				}
			}
		}
		Log.i(TAG, "----END:bitmap copied");
	}

}
