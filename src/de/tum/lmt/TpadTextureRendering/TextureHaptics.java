package de.tum.lmt.TpadTextureRendering;


import ioio.lib.spi.Log;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Random;

import org.opencv.core.Mat;

import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadService;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**  
*
* This class takes care of all tactile feedback related calculations.
* The haptic bitmaps for the visual display can be found under the path /sdcard/HapticDataNew/  . This path is defined in ConstantsClass (also check the ConstantsClass variables for other fixed paths)
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/



public class TextureHaptics  implements OnTouchEventListener
{
	private final String TAG 					= "TextureHaptics";

	private Context mContext 					= null;	
	private TextureModel mTextureModel 			= null;
	private Texture mCurrentTexture				= null;	
	
	
	// Local reference to TPad object
	private TPad mTpad 							= null;		
	// Prediction "Horizon", that is the number of samples needed to extrapolate
	// finger position until the next position is taken
	public static final int PREDICT_HORIZON 	= (int) (TPadService.OUTPUT_SAMPLE_RATE * (.020f)); // 125 samples, 20ms @ sample rate output	
	public int mRenderType 						= ConstantsClass.DIRECT;
	private int mHeight 						= 0;
	private int mWidth							= 0;
	public Bitmap mHapticBitmap					= null;
	public Bitmap mHapticBitmapScaled			= null;
	public float mHapticScaleFactorOriginal		= 1.0f;
	public float mHapticScaleFactorInput		= 1.0f;		
	
	
	
	
	
	// Array for holding the extrapolated pixel values
	public static float[] finalFrictionPixels 	= new float[PREDICT_HORIZON];
	public static float[] macroRoughnessPixels  = new float[PREDICT_HORIZON];
	public static float[] microRoughnessPixels  = new float[PREDICT_HORIZON];
	public static float[] dynamicFrictionPixels = new float[PREDICT_HORIZON];	

	
	

	


	public boolean mUseWav 						= true;
	public WaveReader mWavReader 				= null;
	private double[] mLeftChannel; 
	private long mWavCounter 					= 0L;
	private long mWavSize;
	
	/**
	 * The constructer sets all initial variables used for the tactile display.
	 * @param
	 * @return
	 * 
	 * 
	 */
	public TextureHaptics(Context context,TextureModel texModel, TPad tpad, AttributeSet attrs) 
	{
		mContext = context;
		
		mHapticScaleFactorOriginal 	= 1.0f;
		mHapticScaleFactorInput 	= 1.0f;	
		mTextureModel 				= texModel;
		mTpad 						= tpad;
		
		this.initWaveData();
		mLeftChannel = new double[mWavReader.getSize()];
		
	}
	/**
	 * Set current texture to passed one.
	 * @param Texture
	 * @return
	 * 
	 * 
	 */
	public void setTexture(Texture newTexture) 
	{
		this.mCurrentTexture = newTexture;
		
		if(mUseWav)
		{
			this.getWavSoundFromFile();
			mLeftChannel = new double[mWavReader.getSize()];
			mWavSize = mWavReader.getSize();
		}
		
		if(this.mCurrentTexture != null) 
		{
			setHapticBitmap(mCurrentTexture.createHapticBitmapName());
			
		}
		else 
		{
			mHapticBitmap 			= null;
			mHapticBitmapScaled 	= null;
		}
	}
	public void setTexture(Texture newTexture, String path) 
	{
		mCurrentTexture = newTexture;
		if(mUseWav)
		{
			this.getWavSoundFromFile();
			mLeftChannel = new double[mWavReader.getSize()];
			mWavSize = mWavReader.getSize();
		}
		if(mCurrentTexture != null) 
		{
			setHapticBitmap(path);
		}
		else 
		{
			mHapticBitmap 			= null;
			mHapticBitmapScaled 	= null;
			
		}
	}
	/**
	 * Set tpad object, passed by TextureModel fragment.
	 * @param tpad
	 */
	public void setTpad(TPad tpad) 
	{
		mTpad = tpad;
	}



	
	/**
	 * This method sets the underlying haptic bitmap with the original scaling factor.
	 * @param Bitmap to be haptically displayed.
	 * @return
	 * 
	 * 
	 */
	public void setHapticBitmap(Bitmap bmp) 
	{
		mHapticScaleFactorOriginal = (float) bmp.getWidth() / (float) mWidth;
		mHapticScaleFactorInput = 1.0f;
			
		// Create new bitmap from a copy of the reference. This ensures the
		// reference copy won't be used, and it can then be destroyed.
		mHapticBitmap = null;
		mHapticBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);

		// Create a scaled bitmap
		resetScaleFactor(mWidth);
		Log.i(TAG, "Macro roughness image rescaled to " + " w: " +  mHapticBitmapScaled.getWidth() + "h: " + mHapticBitmapScaled.getHeight());

		mTextureModel.imageDisplay.invalidate();
	}	
	/**
	 * 
	 * @param File name of the Haptic bitmap to be set.
	 * @return
	 * 
	 * 
	 * This function sets the friction map bitmap according to a passed filename. This file is stored on the sdcard in the tpad phone, see constantsClass for more details
	 */
	public void setHapticBitmap(String filename) 
	{
		Bitmap newBitmap;
		BitmapFactory.Options options 	= new BitmapFactory.Options();
		options.inPreferredConfig 		= Bitmap.Config.ARGB_8888;
		
		try 
		{
			newBitmap = BitmapFactory.decodeFile(filename, options);
			Log.i(TAG, "Macro roughness image loaded!" + filename);
			Log.i(TAG, "w " + newBitmap.getWidth() + "h " + newBitmap.getHeight());
			
			// Set this as the haptic bitmap
			this.setHapticBitmap(newBitmap);
		}
		catch (Exception e) 
		{
			Log.i(TAG, "Macro roughness image not found!");
		}
		
		
	}


	
	
	
	
	

	/**
	 * 
	 * @param visualScaler
	 * @return
	 * 
	 * 
	 * This function is constantly called by Draw() in TextureImage. 
	 */
	public void synchronizeHapticScaleFactorWithVisualScaleFactor(float visualScaler)
	{
		mHapticScaleFactorInput = visualScaler;
	}
	/**
	 * Scale a bitmap with regard to a passed scale factor.
	 * @param Bitmap and scaling factor
	 * @return
	 * 
	 * 
	 */	
	private void createScaledBitmap(Bitmap bit, float factor) 
	{
		if(bit != null) 
		{
			try 
			{
				mHapticBitmapScaled = Bitmap.createScaledBitmap(bit, (int) (bit.getWidth() / factor), (int) (bit.getHeight() / factor), false);
			}
			catch (Exception e) 
			{
				Log.e(TAG, "Haptic bitmap rescale error: " + e.toString());
			}
		}
		else 
		{
			mHapticBitmapScaled = Bitmap.createBitmap(ConstantsClass.DEFAULT_IMAGE_WIDTH , ConstantsClass.DEFAULT_IMAGE_HEIGHT, Bitmap.Config.ARGB_8888);
		}
	}
	/**
	 * Reset the scale factor and call createScaledBitmap.
	 * @param width
	 * @return
	 * 
	 * 
	 */	
	public void resetScaleFactor(int width) 
	{
		// Store the width of the view
		mWidth = width;
		
		if(mHapticBitmap != null) 
		{
			mHapticScaleFactorOriginal = (float) mHapticBitmap.getWidth() / (float) width;
			createScaledBitmap(mHapticBitmap, mHapticScaleFactorOriginal);
		}
		
	}
	/**
	 * 
	 * This method is called in TextureImage. The mViewRectangle gets the canvas dimensions in the onDraw() method. The view Rectangle is important in our application, whenever the user tilts the view that we check if the borders are still correct.
	 * We also have to synchronize this with the haptic display part what is actually done in this method here.
	 * @param Rectangular area r
	 * @return
	 * 
	 * 
	 * 
	 */	
	public void scale(Rect r) 
	{

		int left 	= (int) (r.left 	* mHapticScaleFactorOriginal);
		int right 	= (int) (r.right 	* mHapticScaleFactorOriginal);
		int top 	= (int) (r.top 		* mHapticScaleFactorOriginal);
		int bottom 	= (int) (r.bottom 	* mHapticScaleFactorOriginal);
		
		Bitmap tempBit;
		if(mHapticBitmap != null) 
		{
			// Check for boundaries
			if(left < 0) left = 0;
			if(top  < 0) top = 0;
			if(right   > mHapticBitmap.getWidth())
				right  = mHapticBitmap.getWidth();
			if(bottom  > mHapticBitmap.getHeight())
				bottom = mHapticBitmap.getHeight();

		
			// Create the new bitmap: it is a high-res cut-out copy from the original full scale bitmap
			try
			{
				tempBit = Bitmap.createBitmap(this.mHapticBitmap, left, top, (right-left), (bottom-top));
				mHapticBitmapScaled = Bitmap.createScaledBitmap(tempBit, (int) (tempBit.getWidth() / mHapticScaleFactorOriginal * mHapticScaleFactorInput), (int) (tempBit.getHeight() / mHapticScaleFactorOriginal * mHapticScaleFactorInput), false);	
				tempBit.recycle();
			}
			catch (Exception e) 
			{
				Log.e(TAG, "Haptic bitmap resize error: " + e.toString());
			}
			Log.i(TAG,"Scale original: "+ mHapticScaleFactorOriginal + " scale input: " + mHapticScaleFactorInput);
		}
	}
	/**
	 * Call garbage collection explicitly for the haptic bitmap. 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */	
	public void recycleImages() 
	{
		if(mHapticBitmapScaled != null)
			mHapticBitmapScaled.recycle();
		
		if(mHapticBitmap != null)
			mHapticBitmap.recycle();
	}
	
	
	
	

	

	
	
	
	
	/**
	 * Haptic display for first contact with screen.
	 * @param
	 * @return
	 * 
	 * 
	 */	
	public void onFingerDownEvent(double vx, double vy, double px, double py) 
	{	
		/**
		 * You can also send a predefined pattern to the tpad phone with the sendVibration() function. 
		 * Look into the TPad Lib!
		 * 
		 * 
		 * mTpad.sendVibration(1, 500.0f, 100.0f);
		 * 
		 */
	}
	
	/**
	 * Main calculation part for rendering tactile feedback to the user. We apply a linear combination of micro/macroscopic roughness and a general layer of friction. 
	 * THe tpad expect n next pixel values (0 = off  - 255 = fully enabled ultrasonic device) to generate tactile feedback. It predicts the user movement for these n upcoming values and 
	 * sets its internal friction value according to the array we give:  mTpad.sendFrictionBuffer(finalFrictionPixels);
	 * @param
	 * @return
	 * 
	 * 
	 */	
	public void onFingerMoveEvent(double vx, double vy, double px, double py)
	{
		
		
		// if we have a valid texture loaded and android detects only one finger moving...
		 if( (mTextureModel.mIsTextureLoaded) && (mTextureModel.mCurrentInteractionMode == ConstantsClass.EXPLORE)) 
		 {
		
			 // PREDICT_HORIZON contains the number of pixels, which ultrasonic values we will predict
			 
			 // ***********************************
			 // Dynamic Friction CALCULATION
			 // ***********************************	
			 for(int i = 0; i < PREDICT_HORIZON; i++)
			 {
				 dynamicFrictionPixels[i] = mCurrentTexture.mAmplFric;
			 }


			 // ***********************************
			 // MICRO ROUGHNESS CALCULATION
			 // ***********************************
			 for(int i = 0; i < PREDICT_HORIZON; i++)
			 {
				 if(mUseWav)
				 {
					 mLeftChannel = mWavReader.getData();
					 mWavCounter++;
					 if( mWavCounter >= mWavSize-PREDICT_HORIZON)
					 {
						 mWavCounter = 0;
						 
					 }
					 float microRoughnessIntensity = CalculateMicroscopicRoughnessAmplitude((float)mTextureModel.getCurrentVelocity(),1) * mCurrentTexture.mAmplMicroRough;
					 float microRoughness = MicroscopicRoughnessRandomGenerator(microRoughnessIntensity, mCurrentTexture.mMicroRoughDistribution);
					 
					 microRoughnessPixels[i] = 0.5f*mCurrentTexture.mAmplMicroRough * (ConstantsClass.wavScaler * Math.abs((float)mLeftChannel[(int) (mWavCounter+i)]) + microRoughness);
					 
					 
					// Log.i("WAV", "Fric:" + microRoughnessPixels[i]);

				 }
				 else
				 {
					 float microRoughnessIntensity = CalculateMicroscopicRoughnessAmplitude((float)mTextureModel.getCurrentVelocity(),1) * mCurrentTexture.mAmplMicroRough;
					 float microRoughness = MicroscopicRoughnessRandomGenerator(microRoughnessIntensity, mCurrentTexture.mMicroRoughDistribution);

					 microRoughnessPixels[i] = mCurrentTexture.mAmplMicroRough * microRoughness;
					 //Log.i("WAV", "#");
				 }
			 }


			 // ***********************************
			 // MACRO ROUGHNESS CALCULATION
			 // ***********************************
			 // Call prediction algorithm below. This function computes the
			 // proper extrapolation of friction values and automatically updates
			 // predictedPixels with these values
			 predictPixels(vx, vy, px, py);

			 /*
			  * Now add micro and macro roughness and friction 
			  */
			 
			 
			 


			 for(int i = 0; i < PREDICT_HORIZON; i++)
			 {
				 finalFrictionPixels[i] = mCurrentTexture.mWeightMacroRough * macroRoughnessPixels[i] + mCurrentTexture.mWeightMicroRough * microRoughnessPixels[i] + mCurrentTexture.mWeightFric * dynamicFrictionPixels[i];
					 
				 //finalFrictionPixels[i] = 0.4f * macroRoughnessPixels[i] + 0.6f * microRoughnessPixels[i] ;
			 }				 
				 
			 

			 
			 // should not happen, but to ensure correct friction range:
			 if(finalFrictionPixels[0] < 0) 
				finalFrictionPixels[0] = 0;
			 if(finalFrictionPixels[0] > 0.99f) 
				finalFrictionPixels[0] = 0.99f;
			 //Log.i("FRICTION", "Count: " + String.valueOf(mWavCounter) + " Wav value: "+String.valueOf(finalFrictionPixels[0]));			 
			 
			 
			 
			 /*for(int i = 0; i < PREDICT_HORIZON; i++) //
			 {
				 finalFrictionPixels[i] =  mCurrentTexture.mWeightMacroRough * macroRoughnessPixels[i] + mCurrentTexture.mWeightMicroRough * microRoughnessPixels[i] +  mCurrentTexture.mWeightFric       * dynamicFrictionPixels[i];
				 // should not happen, but to ensure correct friction range:
				 if(finalFrictionPixels[i] < 0) 
					finalFrictionPixels[i] = 0;
				 if(finalFrictionPixels[i] > 0.99f) 
					finalFrictionPixels[i] = 0.99f;
			 }*/

			 try 
			 {

				 // Send the predicted values to the tpad as an array
				 mTpad.sendFrictionBuffer(finalFrictionPixels);
				 //Log.i("FRICTION", "Friction: "+String.valueOf(finalFrictionPixels[0]));
			 }
			 catch (NullPointerException ex) 
			 {
				 Log.e("FrictionMapView", "NullPointerException encountered when sending predictedPixels to Tpad");
				 if(finalFrictionPixels == null)
					 Log.e("TPad", "FrictionMapView Error: predictedPixels is null, causing NullPointerException and preventing haptics");
				 if(mTpad == null)
					 Log.e("TPad", "FrictionMapView Error: Tpad is not set, or is set to null, causing NullPointerException and preventing haptics");
			 } 
		 }
	}
	/**
	 * Always disable the tpad phone capabilities if the user leaves the screen!
	 * @param
	 * @return
	 * 
	 * 
	 */	
	public void onFingerUpEvent(double vx, double vy, double px, double py) 
	{
		// Turn off TPad when user lifts finger
		try 
		{ 
			 mTpad.sendFriction(0.0f); 
			 mTpad.turnOff();
		}
		catch (NullPointerException ex) 
		{  
			Log.e(TAG, "TPad is not initialized (null)"); 
		}
		 
		try 
		{ 
			mTextureModel.mVibrator.cancel(); 
		}
		catch (NullPointerException ex) 
		{  
			Log.e(TAG, "Vibrator is not initialized (null)"); 
		}
	
	}
	
	
	

	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	* Main extrapolation algorithm used to calculate upsampled friction values
	* 
	* 
	*/
	
	public void predictPixels(double vx, double vy, double px, double py) 
	{
		// Check if there is a haptic bitmap set
		if(mCurrentTexture == null || mHapticBitmapScaled == null) 
		{
			for (int i = 0; i < macroRoughnessPixels.length; i++) 
			{
				macroRoughnessPixels[i] = 0.0f;
			}
			return;
		}
		
		
		// Local friction values
		float friction = 0;

		// Local x,y values, based on most recent px, py
		int x = (int) px;
		int y = (int) py;

		// A frequency scaling factor to ensure we are producing the correct
		// number of samples to be played back
		float freqScaleFactor = (float) (1 / (TPadService.OUTPUT_SAMPLE_RATE / 1000.));

		// Main extrapolation loop. This is where the extrapolated data is
		// produced
		for (int i = 0; i < macroRoughnessPixels.length; i++) 
		{

			// 1st order hold in x direction
			x = (int) (px + vx * i * freqScaleFactor);

			// Ensure we are not going off the edge of the bitmap with our
			// extrapolated point
			if (x >= mHapticBitmapScaled.getWidth())
			{
				x = mHapticBitmapScaled.getWidth() - 1;
			} 
			else if (x < 0)
				x = 0;

			// 1st order hold in y direction
			y = (int) (py + vy * i * freqScaleFactor);

			// Ensure we are not going off the edge of the bitmap with our
			// extrapolated point
			if (y >= mHapticBitmapScaled.getHeight()) 
			{
				y = mHapticBitmapScaled.getHeight() - 1;
			} 
			else if (y < 0)
				y = 0;

			switch (mRenderType)
			{
			case ConstantsClass.DIRECT:
				// Get the pixel value at this predicted position, convert it to
				// a friction value, and store it
				friction = pixelToFriction(mHapticBitmapScaled.getPixel(x, y));
				break;
			
			case ConstantsClass.GRADIENT:
				float vAvgx;
				float vAvgy;
				float vAvgMag;

				int vals = 0;
				//vals = gradMatx.get(y, x);
				vals = mHapticBitmapScaled.getPixel(x, y);
				int colorint = (int) vals;
				float getX = (colorint) / 255;

				vals = mHapticBitmapScaled.getPixel(x, y);
				colorint = (int) vals;
				float getY = (colorint) / 255;

				vAvgMag = (float) Math.sqrt(vx * vx + vy * vy);
				vAvgx = (float) (vx / vAvgMag);
				vAvgy = (float) (vy / vAvgMag);

				float scale = (float) (Math.sqrt(2.)/2);

				friction = scale * (-1f * ((getX - .5f) * vAvgx + (getY - .5f) * vAvgy)) + .5f;

				break;

			}

			// Save the stored friction value into the buffer that will be sent
			// to the TPad to be played back as real-time as possible
			macroRoughnessPixels[i] = mCurrentTexture.mAmplMacroRough * friction;
		}

	}

	/**
	 * 
	 * @param 
	 * @return 
	 * 
	 * 
	 * Main mapping to go from a pixel color to a friction value. OVERRIDE THIS
	 * FUNCTION FOR NEW MAPPINGS FROM COLOR TO FRICTION
	 */
	public float pixelToFriction(int pixel)
	{
		// Setup a Hue, Saturation, Value matrix
		float[] hsv = new float[3];

		// Convert the RGB color in to HSV data and store it
		Color.colorToHSV(pixel, hsv);

		// Return the Value of the color, which, generally, corresponds to the
		// grayscale value of the color from 0-1
		return hsv[2];
	}
	
	
	

	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 */
	private float CalculateMacroscopicRoughnessAmplitude( float macroscopicScaleFactor)
	{
		float macroIntensity = macroscopicScaleFactor;
		return macroIntensity;		
	}
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */
	public float CalculateMicroscopicRoughnessAmplitude(float velocity, float microscopicScaleFactor)
	{	
		if(velocity < 1) 
		{
			velocity = 1;
		}
		float microIntensity = 1.0f; //(float) (0.5f + Math.log10(velocity));

		if(microIntensity > 1) 
		{
			microIntensity = 1;
		}
		return microIntensity;		
	}
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 * microIntensity is between 0 and 1
	 * probabilityScaler is between 0.1 and inf
	 * 
	 * */
	public float MicroscopicRoughnessRandomGenerator(float microIntensity, float probabilityScaler)
	{
		float microscopicRoughness = 0.0f;
		
		Random r = new Random();
		microscopicRoughness = r.nextFloat();

		
		if(microscopicRoughness > 1) microscopicRoughness = 1;
		if(microscopicRoughness < 0) microscopicRoughness = 0;
			
		if(microscopicRoughness > probabilityScaler) microscopicRoughness = 1;
		else microscopicRoughness = 0;
			
		
		microscopicRoughness = microIntensity * microscopicRoughness;

		
		return microscopicRoughness;
	}
	
	
	/**
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */
	public float MicroscopicRoughnessRandomGenerator2(float microIntensity, float probabilityScaler)
	{
		float microscopicRoughness = 0.0f;
		int max = 1;
		int min = 0;
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    microscopicRoughness = (float)(microIntensity * (rand.nextInt((max - min) + 1) + min));
	    
		return microscopicRoughness;
	}	
	
	
	
	
	

	
	private void initWaveData() 
	{
		Log.i(TAG, "WAVE started");

		
		Log.i("WAV", "PATH to WAV:" + ConstantsClass.wavFile);
		InputStream is = null;
		try 
		{
		        is = new FileInputStream( ConstantsClass.wavFile);
		} 
		catch (FileNotFoundException e) 
		{
		        e.printStackTrace();	       
		}
		catch (IOException e) 
		{
		        e.printStackTrace();
		}
	
		try
		{
			Log.i("WAV", "Sound calling wave reader");
			mWavReader = new WaveReader(is);
			Log.i("WAV", "Sound finished calling wave reader, number of samples " + mWavReader.getSize());
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		catch (NullPointerException ex) 
		{
			Log.e("WAV", "WAV NullPointerException1!");
		}
	}
	private void getWavSoundFromFile() 
	{
		Log.i(TAG, "Sound started reading");

		String wavFile = ConstantsClass.wavFilePrefix + mCurrentTexture.mTextureName + ConstantsClass.wavFileSuffix;
		Log.i("WAV", "PATH to WAV:" + wavFile);
		InputStream is = null;
	
		try 
		{
		        is = new FileInputStream(wavFile);
		} 
		catch (FileNotFoundException e) 
		{
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		        wavFile = ConstantsClass.wavFile;
		        try {
					is = new FileInputStream(wavFile);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		       
		}
		catch (IOException e) 
		{
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		        wavFile = ConstantsClass.wavFile;
		        try {
					is = new FileInputStream(wavFile);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		 }
	
		try
		{
			Log.i("WAV", "Sound calling wave reader");
			mWavReader = new WaveReader(is);
			
			Log.i("WAV", "Sound finished calling wave reader, number of samples " + mWavReader.getSize());
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NullPointerException ex) 
		{
			Log.e("WAV", "WAV NullPointerException  2!");
		}
	}
	
	
	
}
