package de.tum.lmt.TpadTextureRendering;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import android.util.Log;



/** Description of Class 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class PPMM_Server 
{
	Thread mThread;
	int mPort;
	
	
	public class MyServer extends Thread
	{
		private static final String TAG = "PPMM_Server";
		private ServerSocket mServerSocket;		
		private Socket mSocket;
		DataInputStream mIn;
		DataOutputStream mOut;
		Socket mServer; 
		
		public MyServer(int mPort) throws IOException
		{
		      mServerSocket = new ServerSocket(mPort);
		      mServerSocket.setSoTimeout(10000);
		}

		public void run()
		{
			while(true)
			{
				try 
				{
					Log.i(TAG,"Waiting for client on port " + mServerSocket.getLocalPort() + "...");
			        mServer = mServerSocket.accept();
			        Log.i(TAG,"Just connected to "+ mServer.getRemoteSocketAddress());
			        mIn = new DataInputStream(mServer.getInputStream());
			        Log.i(TAG,mIn.readUTF());
			        DataOutputStream out = new DataOutputStream(mServer.getOutputStream());
			        out.writeUTF("Thank you for connecting to "  + mServer.getLocalSocketAddress() + "\nGoodbye!");
			        mServer.close();
				} 
				catch (SocketTimeoutException s) 
				{
					s.printStackTrace();
				}
				catch(IOException e)
				{}
			}
		}
	};
	
	public PPMM_Server() throws IOException
	{
		mPort = 12345;
		try 
		{
			mThread = new MyServer(mPort);
			mThread.start();
		}
		catch(IOException e)
		{}
	}
	
}
