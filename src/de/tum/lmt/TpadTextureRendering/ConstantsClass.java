package de.tum.lmt.TpadTextureRendering;

import org.opencv.core.CvType;

import android.os.Environment;

/** 
*
* This class is supposed to store all constants used throughout the application. Most important, it contains all the file paths to the used image/sound resources.
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class ConstantsClass
{
	public static final String home 								= "/sdcard/";//Environment.getExternalStorageDirectory().getAbsolutePath();
	public static final String folder 								= home + "/HapticData/"; //"/sdcard/HapticData/"; 
	public static final String folderMacroImage 					= home + "HapticDataNew/";
	public static final String folderDisplayImage 					= home + "ImageDataNew/";
	public static final String folderSoundData 						= home + "SoundDataNew/";
	public static final String bluetoothPath 						= home + "/bluetooth/";
//	public static final String ROUGHNESS_PREFIX 					= "macro";
	public static final String ROUGHNESS_PREFIX 					= "";
	public static final String SOUND_SUFFIX 						= "_soundtpad.wav";
	public static final String SOUND_SUFFIX_IMPACT					= "_impact.wav";
	public static final String HAPTIC_IMAGE_SUFFIX					= ".jpg";	
	
	public static final int HAPTIC_FREQUENCY 						= 38780;//hz
	public static final int DIRECT 									= 1;
	public static final int GRADIENT 								= 2;
	public static final double DISTORTION_K							= -0.000006;//-0.00001
	public static final int NEIGHBORHOOD_SIZE_TO_DISTORT 			= 250;//80;
	public static final long TIMEOUT_LAST_TOUCH_MS 					= 30;// 40
	public static final int NEIGHBORHOOD_SIZE_UNCHANGED_DISTORT 	= 5;//35;
	
	
	protected static final int REQ_CODE_HOME_SCREEN 				= 8;	
	protected static final int REQ_CODE_SCAN_ACTIVITY 				= 7;	
	protected static final int REQ_CODE_HELP_ACTIVITY 				= 6;	
	protected static final int REQ_CODE_INFO_ACTIVITY 				= 5;	
	protected static final int REQ_CODE_PARAMETER_ACTIVITY 			= 4;
	protected static final int REQ_CODE_PICK_IMAGES 				= 3;
	protected static final int REQ_CODE_PICK_IMAGE 					= 2;
	protected static final int REQUEST_TAKE_PHOTO 					= 1;
	protected static final int TAKE_PICTURE 						= 0;
	protected static final int REQ_CODE_NONE 						= -1;
	

	private final int type_image 									= CvType.CV_8UC1; // this constant is used for b/w images, change to CvType.CV_8UC4 for color
	
	
	public static final int EXPLORE 								= 0; // Explore the current texture
	public static final int CHANGE 									= 1; // Drag/pan the texture
	public static final int ZOOM									= 2; // Zoom into the texture
	
	
	
	public static final int DEFAULT_IMAGE_WIDTH 					= 2000;
	public static final int DEFAULT_IMAGE_HEIGHT					= 3800;
		
	
	public static final float wavScaler 							= 3;
	public static final String wavFile 								= "/sdcard/wavSignals/01_carpet_10000Hz.wav";
	public static final String wavFilePrefix 						= "/sdcard/wavSignals/";
	public static final String wavFileSuffix 						= "_10000Hz.wav";
	
}