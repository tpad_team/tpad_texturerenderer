package de.tum.lmt.TpadTextureRendering;

import de.tum.lmt.textureRendering.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;



/** 
*
*
* This activity shows a configuration view, where the user may change manually haptic feedback parameters such as microscopic roughness etc. It will temporarily change the mCurrentTexture settings, if the user reloads another texture model, everything will be reset to the according prestored texture model.

*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class ParametersActivity extends Activity 
{

	private SeekBar mSeekBarMacroAmpl 					= null;    
	private SeekBar mSeekBarMicroAmpl 					= null;    
	private SeekBar mSeekBarFrictionAmpl 				= null;    	
	private SeekBar mSeekBarMacroWeight					= null;    	
	private SeekBar mSeekBarMicroWeight	 				= null; 
	private SeekBar mSeekBarFrictionWeight 				= null;
	private SeekBar mSeekBarMicroDistr 					= null;    	
	private SeekBar mSeekBarHardness 					= null; 
	private SeekBar mSeekBarDuration 					= null;
    private CheckBox mCheckBoxDisplayHapticImage 		= null;
    private CheckBox mCheckBoxDisplayImageDistortion 	= null;
	
	private Texture mCurrentTexture 					= null;
	private int mMacroAmpl 								= 50;    
	private int mMicroAmpl 								= 50;   
	private int mFrictionAmpl 							= 50;  
	
	private int mMacroWeight 							= 34;    	
	private int mMicroWeight 							= 33; 
	private int mFrictionWeight							= 33;
	
	private int mHardness 								= 100;
	private int mDuration 								= 100;
	private int mMicroDistr								= 50;
	
	private boolean mDisplayHapticImage 				= false;
	private boolean mDisplayImageDistortion 			= false;
	
	/**
	 * 
	 * Find the seekBar rederences and install according listeners to get seekbar progress values.
	 * 
	 * @param Bundle savedInstanceState
	 * @return 
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parameters);
		
//		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		Intent intent=this.getIntent();
		Bundle bundle=intent.getExtras();
		mCurrentTexture = (Texture)bundle.getSerializable("CurrentTexture");
		
		mMacroAmpl 		= (int)(100.0f*mCurrentTexture.mAmplMacroRough);    
		mMicroAmpl 		= (int)(100.0f*mCurrentTexture.mAmplMicroRough);    
		mFrictionAmpl 	= (int)(100.0f*mCurrentTexture.mAmplFric);    	
		mMacroWeight 	= (int)(100.0f*mCurrentTexture.mWeightMacroRough);    	
		mMicroWeight 	= (int)(100.0f*mCurrentTexture.mWeightMicroRough); 
		mFrictionWeight = (int)(100.0f*mCurrentTexture.mWeightFric);
		mHardness 		= (int)(100.0f*mCurrentTexture.mAmplHardness);
		mDuration 		= (int)((float)mCurrentTexture.mDurationHardness/3);
		mMicroDistr		= (int)(100.0f*mCurrentTexture.mMicroRoughDistribution);
		mDisplayHapticImage = bundle.getBoolean("HapticDisplay");
		//mDisplayImageDistortion = bundle.getBoolean("DistortionDisplay");
		
		mSeekBarMacroAmpl		=(SeekBar) findViewById(R.id.seekBarMacroAmpl);    
		mSeekBarMicroAmpl		=(SeekBar) findViewById(R.id.seekBarMicroAmpl);    
		mSeekBarFrictionAmpl	=(SeekBar) findViewById(R.id.seekBarFrictionAmpl);    	
		mSeekBarMacroWeight		=(SeekBar) findViewById(R.id.seekBarMacroWeight);    	
		mSeekBarMicroWeight		=(SeekBar) findViewById(R.id.seekBarMicroWeight);  	
		mSeekBarFrictionWeight	=(SeekBar) findViewById(R.id.seekBarFrictionWeight); 
		mSeekBarMicroDistr  	=(SeekBar) findViewById(R.id.seekBarMicroDistribution);     	
		mSeekBarHardness 		=(SeekBar) findViewById(R.id.seekBarHardness);   
		mSeekBarDuration		=(SeekBar) findViewById(R.id.seekBarDuration);   
		
		mCheckBoxDisplayHapticImage = (CheckBox) findViewById(R.id.displayHapticImage);
		mCheckBoxDisplayImageDistortion  = (CheckBox) findViewById(R.id.displayVisualDistortion);
		
		
		
		mSeekBarMacroWeight.setProgress(mMacroWeight);	
		mSeekBarMicroWeight.setProgress(mMicroWeight);	
		mSeekBarFrictionWeight.setProgress(mFrictionWeight);
		
		mSeekBarMacroAmpl.setProgress(mMacroAmpl);
		mSeekBarMicroAmpl.setProgress(mMicroAmpl);
		mSeekBarFrictionAmpl.setProgress(mFrictionAmpl);
		
		mSeekBarMicroDistr.setProgress(mMicroDistr);   	
		mSeekBarHardness.setProgress(mHardness);
		mSeekBarDuration.setProgress(mDuration);
		
		mCheckBoxDisplayHapticImage.setChecked(mDisplayHapticImage);
		mCheckBoxDisplayImageDistortion.setChecked(mDisplayImageDistortion);
		
		
		
		mSeekBarMacroAmpl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				generateIntentWithParameters();
				mSeekBarMacroAmpl.setProgress(mMacroAmpl);
			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mMacroAmpl = progress;
			}
		});
		mSeekBarMicroAmpl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				generateIntentWithParameters();				
				mSeekBarMicroAmpl.setProgress(mMicroAmpl);
			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mMicroAmpl = progress;
			}
		});
		
		
		
		
		mSeekBarFrictionAmpl.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				generateIntentWithParameters();  
				mSeekBarFrictionAmpl.setProgress(mFrictionAmpl);
			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mFrictionAmpl = progress;
			}
		});
		
		
		
		
		mSeekBarMacroWeight.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				mSeekBarMicroWeight.setProgress(100    - mMacroWeight - mFrictionWeight);
				mSeekBarFrictionWeight.setProgress(100 - mMacroWeight - mMicroWeight);		
				generateIntentWithParameters();  
			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mMacroWeight = progress;


			}
		});
		
		
		
		
		mSeekBarMicroWeight.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				mSeekBarMacroWeight.setProgress(100    - mMicroWeight - mFrictionWeight);
				mSeekBarFrictionWeight.setProgress(100 - mMacroWeight - mMicroWeight);	
				generateIntentWithParameters();
			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mMicroWeight = progress;
				

			}
		});
		
		
		
		
		mSeekBarFrictionWeight.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				mSeekBarMacroWeight.setProgress(100 - mMicroWeight - mFrictionWeight);
				mSeekBarMicroWeight.setProgress(100 - mMacroWeight - mFrictionWeight);	
				generateIntentWithParameters();
				
			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mFrictionWeight = progress;
				

			}
		});
		
		
		

		
		mSeekBarMicroDistr.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				mSeekBarMicroDistr.setProgress(mMicroDistr);
				generateIntentWithParameters();

			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mMicroDistr = progress;
				

			}
		});	
		
		
		mSeekBarHardness.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				mSeekBarHardness.setProgress(mHardness);
				generateIntentWithParameters();

			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mHardness = progress;
				

			}
		});		

		mSeekBarDuration.setOnSeekBarChangeListener(new OnSeekBarChangeListener() 
		{
		
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) 
			{
				mSeekBarDuration.setProgress(mDuration);
				
				generateIntentWithParameters();
				


			}	
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) 
			{

			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) 
			{
				mDuration = progress;
				

			}
		});	
		
		mCheckBoxDisplayHapticImage.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		     
				if (((CheckBox) v).isChecked()) {
					mDisplayHapticImage = true; } 
				else {
					mDisplayHapticImage = false; }
				generateIntentWithParameters();
			  }
		});
		
	}

	/**
	 * 
	 *  This intent will be sent back to the caller of the activity (in our case:TextureRendererActivity) with all the parameter values as data content (in extra). However, we are not yet leaving this activity, it can only be left if finish() is called or the user presses the back button
	 * 
	 * @param
	 * @return
	 * 
	 */
	private void generateIntentWithParameters() 
	{
		Intent intent = new Intent();
		intent.putExtra("MacroAmplitude",mMacroAmpl);
		intent.putExtra("MicroAmplitude",mMicroAmpl);
		intent.putExtra("FrictionAmplitude",mFrictionAmpl);
		intent.putExtra("MacroWeight",mMacroWeight);
		intent.putExtra("MicroWeight",mMicroWeight);
		intent.putExtra("FrictionWeight",mFrictionWeight);
		intent.putExtra("MicroDistr",mMicroDistr);
		intent.putExtra("Hardness",mHardness);
		intent.putExtra("Duration",mDuration);
		intent.putExtra("HapticDisplay",mDisplayHapticImage);
		//intent.putExtra("DistortionDisplay",mDisplayImageDistortion);
		
		// 
		setResult(RESULT_OK, intent);    
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.parameters, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		if (id == android.R.id.home)
		{
			finish();			
		}
		return super.onOptionsItemSelected(item);
	}
}
