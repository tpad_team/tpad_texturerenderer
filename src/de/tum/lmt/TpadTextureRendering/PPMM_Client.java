package de.tum.lmt.TpadTextureRendering;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import android.util.Log;


/** Description of Class 
*
*
*
*
*
* @author Dmytro Bobkov
* @author Clemens Schuwerk
* @author Matti Strese
* @version 1.0 
*/
public class PPMM_Client 
{
	private static final String TAG = "Client";
	private String mServerName = "";
	int mPort;
	Socket mClient = null;
	OutputStream mOutStream;
	DataOutputStream mOut;
	
	InputStream mInstream;
	DataInputStream mIn;
	
	boolean mIsConnected = false;
	
	public PPMM_Client()
	{
		mServerName = "localhost";
		mPort = 12345;
	}
	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	public void connectToServer()
	{
		try 
		{
			Log.i(TAG, "Connecting to server...");
			mClient = new Socket(mServerName,mPort);
			Log.i(TAG, "done.");
			mIsConnected = true; 			
			
		} 
		catch (Exception e) 
		{
		

			
		}
	}
	/**
	 * 
	 * @param
	 * @return
	 *
	 */
	public void sendToServer()
	{
		if(mIsConnected)
		{
			try 
			{
				mOutStream = mClient.getOutputStream();

				mOut = new DataOutputStream(mOutStream);
			
				mOut.writeUTF("Data received from: " + mClient.getLocalSocketAddress());
			} 
			catch (IOException e) 
			{

			}			
		}
		else
			this.connectToServer();
	}
	/**
	 * 
	 * @param
	 * @return
	 * 
	 */
	public void receiveFromServer() throws IOException
	{
		if(mIsConnected)
		{
			mInstream = mClient.getInputStream();
		    mIn = new DataInputStream(mInstream);
		    Log.i(TAG,"Server answers: " + mIn.readUTF());
		}
		else
			this.connectToServer();		
	}
	/**
	 * 
	 * @param
	 * @return
	 * 
	 */	
	public void closeClient() throws IOException
	{
		if(mIsConnected && (mClient != null))
		{
			mClient.close();
			mIsConnected = false;
		}		
	}
	
}
